<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\components\BaseARecord;


/**
 * Class Target
 * @package app\models
 */
class Target extends BaseARecord
{

    /**
     *  цели ученика
     */
    const TARGET_PUPIL = 1;

    /**
     * цели учителя
     */
    const TARGET_TEACHER = 2;

    /**
     * Массив целей
     * @var array
     */
    public static $TARGET = [
        self::TARGET_PUPIL => 'цели ученика',
        self::TARGET_TEACHER => 'цели учителя'
    ];

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['name'], 'unique', 'on' => ['create', 'update']],

            [['name'], 'string', 'max' => 255, 'on' => ['create', 'update']],
            [['name', 'lang', 'is_target'], 'required', 'on' => ['create', 'update']],

            [['lang', 'is_target'], 'integer', 'on' => ['create', 'update']],

            [['id', 'name', 'lang', 'is_target'], 'safe', 'on' => ['search']]
        ]);
    }


    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'name' => Yii::t('app', 'Название'),
            'is_target' => Yii::t('app', 'Цель')
        ]);
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lang' => $this->lang,
            'is_target' => $this->is_target
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }

    /**
     * Получение всех тегов
     * @param int $target
     * @return array
     */
    public static function getArrayTargets($lang, $target = self::TARGET_PUPIL)
    {
        return ArrayHelper::map(
            self::find()->where([
                'is_target' => $target,
                'lang' => $lang
            ])->asArray()->all(),
            'id',
            'name'
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardTargets()
    {
        return $this->hasMany(CardTarget::className(), ['target_id' => 'id']);
    }

    /**
     * Промежуточная табличка
     * @return mixed
     */
    public function getCards()
    {
        return $this->hasMany(Card::className(), ['id' => 'card_id'])
            ->via('cardTargets');
    }

}
