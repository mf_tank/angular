<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\components\BaseARecord;

/**
 * Class Order
 * @package app\models
 */
class Order extends BaseARecord
{

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['amount', 'user_id'], 'required', 'on' => ['create', 'update']],
            ['amount', 'number', 'min' => 0, 'on' => ['create', 'update']],
            [['invoice_id'], 'string', 'max' => 255, 'on' => ['create', 'update']],
            [
                ['user_id'], 'exist', 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id'], 'on' => ['create', 'update']
            ],
            [
                'user_id', 'default', 'value' => Yii::$app->getUser()->getId(), 'on' => ['create', 'update']
            ],
            [['id', 'invoice_id', 'amount', 'user_id'], 'safe', 'on' => ['search']],
        ]);
    }

    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'amount' => Yii::t('app', 'Сумма платежа'),
            'invoice_id' => Yii::t('app', 'Яндекс Invoice Id'),
            'user_id' => Yii::t('app', 'Пользователь'),
        ]);
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }

    /**
     * Пользователь
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getFormattedAmount()
    {
        return number_format($this->amount, 2, '.', '');

    }

    /**
     * История
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(OrderHistory::className(), ['order_id' => 'id']);
    }

}
