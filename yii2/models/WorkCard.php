<?php
namespace app\models;


use app\components\BaseARecord;


/**
 * Class WorkCard
 * @package app\models
 */
class WorkCard extends BaseARecord
{

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['work_id', 'card_id', 'time'], 'required', 'on' => ['create', 'update']],

            [
                ['card_id'], 'exist', 'targetClass' => Card::className(),
                'targetAttribute' => ['card_id' => 'id'], 'on' => ['create', 'update']
            ],
            [
                ['work_id'], 'exist', 'targetClass' => Work::className(),
                'targetAttribute' => ['work_id' => 'id'], 'on' => ['create', 'update']
            ],
            [['text'], 'safe', 'on' => ['create', 'update']]
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(Card::className(), ['id' => 'card_id']);
    }

}
