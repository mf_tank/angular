<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\components\BaseARecord;

/**
 * Модель для пользователей системы.
 */
class User extends BaseARecord  implements IdentityInterface
{

    /**
     * Старый пароль
     * @var string
     */
    public $password_old;

    /**
     * Повтор пароля
     * @var sting
     */
    public $password_repeat;

    /**
     * Запомнить меня
     * @var bool
     */
    public $rememberMe = false;

    /**
     * Роль
     * @var array имя роли
     */
    public $roles = [];


    /**
     * Время жизни куки 1 день
     * @var int
     */
    public static $expire = 86400;


    /**
     * Список возможных ролей
     * @var array
     */
    public static $ROLES_ALL = [
        'admin' => 'Администратор',
        'guest' => 'Гость'
    ];

    /**
     * @var object Экземпляр объекта User
     */
    private $_user;

    /**
     * @var array EAuth attributes
     */
    public $profile;


    public $identity;

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['username', 'password'], 'string', 'max' => 255, 'min' => 3, 'on' => ['login']],
            [['username'], 'email', 'on' => ['login']],
            [['username', 'password'], 'required', 'on' => ['login']],
            ['password', 'validatePassword', 'on' => ['login']],


            [['username', 'password'], 'string', 'max' => 255, 'min' => 3, 'on' => ['registration', 'create']],
            [['username', 'password', 'password_repeat'], 'required', 'on' => ['registration', 'create']],
            [['password'], 'compare', 'on' => ['registration', 'create']],
            [['username'], 'email', 'on' => ['registration', 'create']],
            [['username'], 'unique', 'on' => ['registration', 'create']],
            [['roles'], 'safe', 'on' => ['registration', 'create']],


            [['username', 'password'], 'string', 'max' => 255, 'min' => 3, 'on' => ['socNetwork']],
            [['username', 'password'], 'required', 'on' => ['socNetwork']],
            [['username'], 'unique', 'on' => ['socNetwork']],
            [['name', 'img'], 'safe', 'on' => ['socNetwork']],


            [['username'], 'string', 'max' => 255, 'min' => 3, 'on' => ['change']],
            [['username'], 'unique', 'on' => ['change']],
            [['username'], 'required', 'on' => ['change']],
            [['roles'], 'safe', 'on' => ['change']],


            [['password', 'password_old'], 'required', 'on' => ['change_password']],
            ['password_old', 'validatePassword', 'on' => ['change_password']],
            ['password', 'validatePasswordOld', 'on' => ['change_password']],
            ['password', 'compare', 'on' => ['change_password']],
            ['password_repeat', 'safe', 'on' => ['change_password']],

            [['username'], 'required', 'on' => ['update']],
            [['username'], 'unique', 'on' => ['update']],
            [['username'], 'email', 'on' => ['update']],


            [['username', 'password'], 'safe', 'on' => ['social']],
            [['id', 'username', 'name'], 'safe', 'on' => ['search']]
        ]);
    }


    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'name' => Yii::t('app', 'Имя'),
            'img' => Yii::t('app', 'Картинка'),
            'username' => Yii::t('app', 'E-mail'),
            'password' => Yii::t('app', 'Пароль'),
            'auth_key' => Yii::t('app', 'Ключ'),
            'access_token' => Yii::t('app', 'Токен'),
            'password_old' => Yii::t('app', 'Старый пароль'),
            'password_repeat' => Yii::t('app', 'Повтор пароля'),
            'roles' => Yii::t('app', 'Роли'),
            'cards_count' => Yii::t('app', 'Создал методик'),
            'cards_like_count' => Yii::t('app', 'Методик в избранном'),
            'works_count' => Yii::t('app', 'Создал занятий'),
        ]);
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service)
    {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();

        $attributes = [
            'id' => $id,
            'username' => $service->getAttribute('id').'_'.$service->getServiceName(),
            'password' => Yii::$app->getSecurity()->generateRandomString(),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    /**
     * Залогиневание
     * @return bool
     */
    public function login()
    {
        if($this->validate()) {
            $user = $this->getUser();
            return Yii::$app->user->login($user, $this->rememberMe ? 36000 : 3600);
        }
        return false;
    }

    /**
     * Получить пользователя по логину
     * @return User
     */
    public function getUser()
    {
        if(!$this->_user instanceof User) {
            $this->_user = User::findByUsername($this->username);
        }
        return $this->_user;
    }

    /**
     * Список пользователей
     * @return array
     */
    public static function getUsers()
    {
        return ArrayHelper::map(
            User::find()->asArray()->all(),
            'id', 'username'
        );
    }

    /**
     * Создано карточек
     * @return integer
     */
    public function getCards_count()
    {
        return Card::find()->where(['user_id' => $this->id])->count();
    }

    /**
     * Лайков карточек
     * @return integer
     */
    public function getCards_like_count()
    {
        return CardUser::find()->where(['user_id' => $this->id])->count();
    }

    /**
     * Создал занятий
     * @return integer
     */
    public function getWorks_count()
    {
        return Work::find()->where(['user_id' => $this->id])->count();
    }

    /**
     * @param int|string $id
     * @return User|null
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * Валидирование пароля
     * @param $attribute string Имя аттрибута
     */
    public function validatePassword($attribute)
    {
        $user = $this->getUser();
        if (!$user || !Yii::$app->getSecurity()->validatePassword($this->{$attribute},$user->password)) {
            if($attribute == 'password_old') {
                $this->addError('password_old', Yii::t('app', 'Старый пароль должен совпадать'));
            } else {
                $this->addError('password', Yii::t('app', 'Неверное имя пользователя или пароль'));
            }
        }
    }

    /**
     * Валидирование старого пароля
     * @param $attribute string Имя аттрибута
     */
    public function validatePasswordOld($attribute)
    {
        $user = $this->getUser();
        if (!$user || Yii::$app->getSecurity()->validatePassword($this->{$attribute}, $user->password)) {
            $this->addError('password', Yii::t('app', 'Новый пароль не может быть равен старому'));
        }
    }

    /**
     * Возвращает модель пользователя по access_token
     * @param $token
     * @param null $type
     * @return static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Поиск по логину пользователя
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Установка ролей
     * @param $ids array массив ролей
     */
    public function setRoles($ids) {
        $this->roles = $ids;
    }

    /**
     * Список ролей
     * @return array роли пользователя
     */
    public function getRoles()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->getId());
        $result = [];
        foreach($roles as $role) {
            if(array_key_exists($role->name, self::$ROLES_ALL)) {
                $result[] = $role->name;
            }
        }
        return $result;
    }

    /**
     * Получить id пользователя
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить auth_key для пользователя
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Функция валидности поля auth_key
     * @param string $authKey
     * @return boolean если ключ валидный для пользователя
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Событие до сохранения модели
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        if($this->getScenario() == 'change_password') {
            $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * Событие после сохранения модели
     * @return bool|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        if($this->getScenario() == 'change' || $this->getScenario() == 'registration') {
            $auth = Yii::$app->authManager;
            $auth->revokeAll($this->getId());
            foreach($this->roles as $role) {
                $auth->assign(
                    $auth->getRole($role),
                    $this->getId()
                );
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Событие до вставки модели
     * @param $event
     */
    public function beforeInsert($event)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
        $this->access_token = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * Событие после вставки модели
     * @param $event
     */
    public function afterInsert($event)
    {
        $auth = Yii::$app->authManager;
        $auth->assign(
            $auth->getRole('guest'),
            $this->getId()
        );
    }

    /**
     * До удаления
     * @return bool
     */
    public function beforeDelete() {
        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->id);
        return parent::beforeDelete();
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id
        ]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }


    /**
     * Получение всех тегов
     * @return mixed
     */
    public static function getArrayUsers()
    {
        return ArrayHelper::map(
            self::find()->asArray()->all(),
            'id',
            'username'
        );
    }

}
