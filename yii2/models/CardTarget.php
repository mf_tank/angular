<?php
namespace app\models;


use app\components\BaseARecord;


/**
 * Class CardTarget
 * @package app\models
 */
class CardTarget extends BaseARecord
{

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['card_id', 'target_id'], 'required', 'on' => ['create', 'update']],

            [
                ['card_id'], 'exist', 'targetClass' => Card::className(),
                'targetAttribute' => ['card_id' => 'id'], 'on' => ['create', 'update']
            ],
            [
                ['target_id'], 'exist', 'targetClass' => Target::className(),
                'targetAttribute' => ['target_id' => 'id'], 'on' => ['create', 'update']
            ]
        ]);
    }


}
