<?php
namespace app\models;

use Yii;
use app\components\BaseARecord;


/**
 * Class CardText
 * @package app\models
 */
class CardText extends BaseARecord
{

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['text', 'card_id'], 'required', 'on' => ['create', 'update']],

            [
                ['popup_id'], 'exist', 'targetClass' => Popup::className(),
                'targetAttribute' => ['popup_id' => 'id'], 'on' => ['create', 'update']
            ],
            [
                ['card_id'], 'exist', 'targetClass' => Card::className(),
                'targetAttribute' => ['card_id' => 'id'], 'on' => ['create', 'update']
            ]
        ]);
    }


    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'text' => Yii::t('app', 'шаг'),
            'popup_id' => Yii::t('app', 'Подсказка'),
        ]);
    }


    /**
     * Промежуточная табличка
     * @return mixed
     */
    public function getPopup()
    {
        return $this->hasOne(Popup::className(), ['id' => 'popup_id']);
    }

}
