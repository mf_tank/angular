<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\components\BaseARecord;

/**
 * Class OrderHistory
 * @package app\models
 */
class OrderHistory extends BaseARecord
{

    const STATUS_CREATED = 0;
    const STATUS_PAID = 1;

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['order_id', 'status'], 'required', 'on' => ['create', 'update']],
            [
                ['order_id'], 'exist', 'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id'], 'on' => ['create', 'update']
            ],
            [['id', 'order_id', 'status'], 'safe', 'on' => ['search']],
        ]);
    }

    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'status' => Yii::t('app', 'Статус платежа'),
        ]);
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }

    /**
     * Заказ
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

}
