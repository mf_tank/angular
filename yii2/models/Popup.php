<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\components\BaseARecord;


/**
 * Class Popup
 * @package app\models
 */
class Popup extends BaseARecord
{

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['name'], 'unique', 'on' => ['create', 'update']],

            [['name'], 'string', 'max' => 255, 'on' => ['create', 'update']],

            [['text', 'name', 'lang'], 'required', 'on' => ['create', 'update']],

            [['lang'], 'integer', 'on' => ['create', 'update']],

            [['id', 'name', 'lang'], 'safe', 'on' => ['search']]
        ]);
    }


    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'name' => Yii::t('app', 'Имя'),
            'text' => Yii::t('app', 'Текст подсказки'),
        ]);
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lang' => $this->lang
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }

    public function getTextArray (){
        return explode("\r\n", $this->text);
    }

    /**
     * Получение всех тегов
     * @param int $target
     * @return array
     */
    public static function getArrayPopups($lang)
    {
        return ArrayHelper::map(
            self::find()->where([
                'lang' => $lang
            ])->asArray()->all(),
            'id',
            'name'
        );
    }

    /**
     * Возвращаемые поля
     * @return array
     */
    public function fields()
    {
        return ArrayHelper::merge(
            [
                'text_array' => function($model) {
                    return $model->getTextArray();

                },
            ],
            parent::fields()
        );
    }
}
