<?php
namespace app\models;

use Yii;
use app\components\BaseARecord;


/**
 * Class CardUser
 * @package app\models
 */
class CardUser extends BaseARecord
{

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['user_id', 'card_id'], 'required', 'on' => ['create', 'update']],

            [
                ['user_id'], 'exist', 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id'], 'on' => ['create', 'update']
            ],
            [
                ['card_id'], 'exist', 'targetClass' => Card::className(),
                'targetAttribute' => ['card_id' => 'id'], 'on' => ['create', 'update']
            ]
        ]);
    }


    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), []);
    }


}
