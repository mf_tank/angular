<?php
namespace app\models;
use Yii;
use yii\data\ActiveDataProvider;
use app\components\BaseARecord;

/**
 * Class Theses
 * @package app\models
 */
class WorkTheses extends BaseARecord
{
    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['title'], 'required'],
        ]);
    }

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }
}