<?php

namespace app\models\yandex;

use app\models\Order;
use app\models\OrderHistory;
use DateTime;
use app\models\yandex\Utils;
use yii\db\Exception;

class YaMoneyCommonHttpProtocol
{

    private $action;
    private $settings;
    private $order;

    public function __construct($action, Settings $settings, Order $order)
    {
        $this->action = $action;
        $this->settings = $settings;
        $this->order = $order;
    }

    /**
     * Processes "checkOrder" and "paymentAviso" requests.
     * @param array $request payment parameters
     */
    public function processRequest($request)
    {
        \Yii::info("Start " . $this->action);
        \Yii::info("Security type " . $this->settings->SECURITY_TYPE);
        if ($this->settings->SECURITY_TYPE == "MD5") {
            \Yii::info("Request: " . print_r($request, true));
            // If the MD5 checking fails, respond with "1" error code
            if (!$this->checkMD5($request)) {
                $response = $this->buildResponse($this->action, $request['invoiceId'], 1);
                $this->sendResponse($response);
            }
        } else if ($this->settings->SECURITY_TYPE == "PKCS7") {
            // Checking for a certificate sign. If the checking fails, respond with "200" error code.
            if (($request = $this->verifySign()) == null) {
                $response = $this->buildResponse($this->action, null, 200);
                $this->sendResponse($response);
            }
            \Yii::info("Request: " . print_r($request, true));
        }
        $response = null;
        if ($this->action == 'checkOrder') {
            $response = $this->checkOrder($request);
        } else {
            $response = $this->paymentAviso($request);
        }
        $this->sendResponse($response);
    }

    /**
     * CheckOrder request processing.
     * @param  array $request payment parameters
     * @return string         prepared XML response
     */
    private function checkOrder($request)
    {
        $response = null;
        if ($request['orderSumAmount'] != $this->order->formattedAmount) {
            $response = $this->buildResponse($this->action, $request['invoiceId'], 100, "The amount should be " . $this->order->formattedAmount . " rubles.");
        } else {
            $response = $this->buildResponse($this->action, $request['invoiceId'], 0);
        }
        return $response;
    }

    /**
     * PaymentAviso request processing.
     * @param  array $request payment parameters
     * @return string prepared response in XML format
     */
    private function paymentAviso($request)
    {
        $response = null;
        if ($request['orderSumAmount'] != $this->order->formattedAmount) {
            $response = $this->buildResponse($this->action, $request['invoiceId'], 100, "The amount should be " . $this->order->formattedAmount . " rubles.");
        } else {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $this->order->invoice_id = $request['invoiceId'];
                $this->order->save();
                $this->order->user->paid_to = (!$this->order->user->paid_to || $this->order->user->paid_to > time() )? $this->order->user->paid_to + $this->settings->period:time() + $this->settings->period;
                $this->order->user->save();
                $history = new OrderHistory();
                $history->status = OrderHistory::STATUS_PAID;
                $this->order->link('history', $history);
                $transaction->commit();
                return $this->buildResponse($this->action, $request['invoiceId'], 0);
            } catch (Exception $e) {
                $transaction->rollBack();
                \Yii::error($e);
            }
        }
        return $response;
    }


    /**
     * Checking the MD5 sign.
     * @param  array $request payment parameters
     * @return bool true if MD5 hash is correct
     */
    private function checkMD5($request)
    {
        $str = $request['action'] . ";" .
            $this->order->formattedAmount . ";" . $request['orderSumCurrencyPaycash'] . ";" .
            $request['orderSumBankPaycash'] . ";" . $request['shopId'] . ";" .
            $request['invoiceId'] . ";" . trim($request['customerNumber']) . ";" . $this->settings->SHOP_PASSWORD;
        \Yii::info("String to md5: " . $str);
        $md5 = strtoupper(md5($str));
        if ($md5 != strtoupper($request['md5'])) {
            \Yii::warning("Wait for md5:" . $md5 . ", recieved md5: " . $request['md5']);
            return false;
        }
        return true;
    }

    /**
     * Building XML response.
     * @param  string $functionName "checkOrder" or "paymentAviso" string
     * @param  string $invoiceId transaction number
     * @param  string $result_code result code
     * @param  string $message error message. May be null.
     * @return string                prepared XML response
     */
    private function buildResponse($functionName, $invoiceId, $result_code, $message = null)
    {
        try {
            $performedDatetime = Utils::formatDate(new DateTime());
            $response = '<?xml version="1.0" encoding="UTF-8"?><' . $functionName . 'Response performedDatetime="' . $performedDatetime .
                '" code="' . $result_code . '" ' . ($message != null ? 'message="' . $message . '"' : "") . ' invoiceId="' . $invoiceId . '" shopId="' . $this->settings->SHOP_ID . '"/>';
            return $response;
        } catch (\Exception $e) {
            \Yii::error($e);
        }
        return null;
    }

    /**
     * Checking for sign when XML/PKCS#7 scheme is used.
     * @return array if request is successful, returns key-value array of request params, null otherwise.
     */
    private function verifySign()
    {
        $descriptorspec = array(0 => array("pipe", "r"), 1 => array("pipe", "w"), 2 => array("pipe", "w"));
        $certificate = 'yamoney.pem';
        $process = proc_open('openssl smime -verify -inform PEM -nointern -certfile ' . $certificate . ' -CAfile ' . $certificate,
            $descriptorspec, $pipes);
        if (is_resource($process)) {
            // Getting data from request body.
            $data = file_get_contents($this->settings->request_source); // "php://input"
            fwrite($pipes[0], $data);
            fclose($pipes[0]);
            $content = stream_get_contents($pipes[1]);
            fclose($pipes[1]);
            $resCode = proc_close($process);
            if ($resCode != 0) {
                return null;
            } else {
                \Yii::info("Row xml: " . $content);
                $xml = simplexml_load_string($content);
                $array = json_decode(json_encode($xml), TRUE);
                return $array["@attributes"];
            }
        }
        return null;
    }

    private function sendResponse($responseBody)
    {
        \Yii::info("Response: " . $responseBody);
        header("HTTP/1.0 200");
        header("Content-Type: application/xml");
        echo $responseBody;
        exit;
    }
}