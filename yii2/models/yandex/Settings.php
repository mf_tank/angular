<?php


namespace app\models\yandex;


class Settings {
    public $SHOP_PASSWORD;
    public $SECURITY_TYPE;
    public $LOG_FILE;
    public $SHOP_ID;
    public $CURRENCY = 643;
    public $request_source;
    public $mws_cert;
    public $mws_private_key;
    public $mws_cert_password = "123456";
    public $period;
    function __construct($SECURITY_TYPE = "MD5" /* MD5 | PKCS7 */, $request_source = "php://input") {
        $this->SECURITY_TYPE = $SECURITY_TYPE;
        $this->request_source = $request_source;
        $this->SHOP_PASSWORD = \Yii::$app->params['yandex_money']['SHOP_PASSWORD'];
        $this->SHOP_ID = \Yii::$app->params['yandex_money']['shopId'];
        $this->mws_cert = dirname(__FILE__)."/mws/shop.cer";
        $this->mws_private_key = dirname(__FILE__)."/mws/private.key";
        $this->period = time() + 60*60*24*30;
    }
}