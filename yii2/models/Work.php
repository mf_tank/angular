<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\components\BaseARecord;
use app\models\WorkTheses;
use yii\db\Exception;

/**
 * Class Work
 * @package app\models
 */
class Work extends BaseARecord
{

    /**
     * @var string
     */
    public $workCards = [];

    /**
     * @var array
     */
    public $workCardsText = [];

    /**
     * @var object
     */
    public $inputWorkTheses;
    /**
     * @var array
     */
    public $workCardsTime = [];


    const STATUS_WORK = 1;
    const STATUS_VIEW = 2;

    /**
     * @var array
     */
    static $STATUS = [
        self::STATUS_WORK => 'Обычное занятие',
        self::STATUS_VIEW => 'Занятие в примере'
    ];

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['name'], 'unique', 'on' => ['create', 'update']],

            [['name'], 'string', 'max' => 255, 'on' => ['create', 'update']],
            [['name', 'lang', 'time'], 'required', 'on' => ['create', 'update']],

            [
                [
                    'lang', 'time', 'status',
                    'begin_count', 'center_count', 'finish_count'
                ],
                'integer', 'on' => ['create', 'update']
            ],

            [
                ['user_id'], 'exist', 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id'], 'on' => ['create', 'update']
            ],
            [
                ['workCards', 'workCardsText', 'workCardsTime', 'inputWorkTheses', 'workTheses'], 'safe', 'on' => ['create', 'update']
            ],

            [
                ['user_id'], 'default',
                'value' => Yii::$app->getUser()->getIdentity()->getId(),
                'on' => ['create', 'update']
            ],
            [
                ['begin_count', 'center_count', 'finish_count'], 'default', 'value' => 1, 'on' => ['create', 'update']
            ],
            [
                ['status'], 'default', 'value' => self::STATUS_WORK, 'on' => ['create', 'update']
            ],

            [['id', 'name', 'lang'], 'safe', 'on' => ['search']]
        ]);
    }


    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'name' => Yii::t('app', 'Название'),
            'WorkTheses' => Yii::t('app', 'Тезисы'),
            'time' => Yii::t('app', 'Время'),
            'status' => Yii::t('app', 'Статус занятия'),

            'begin_count' => Yii::t('app', '% вначале'),
            'center_count' => Yii::t('app', '% всередине'),
            'finish_count' => Yii::t('app', '% вконце'),
        ]);
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lang' => $this->lang
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }

    /**
     * Событие после сохранения модели
     * @return bool|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            WorkCard::deleteAll('work_id=:work_id', [':work_id' => $this->id]);
            if ($this->workCards) {

                if (is_array($this->workCards)) {
                    foreach ($this->workCards as $workCard) {
                        $currency = new WorkCard(['scenario' => 'create']);
                        $currency->work_id = $this->id;
                        $currency->card_id = $workCard['card_id'];
                        $currency->text = isset($workCard['text'])?$workCard['text']:null;
                        $currency->time = isset($workCard['time'])?$workCard['time']:null;
                        $currency->save();
                    }
                } else {
                    $workCards = json_decode($this->workCards);
                    foreach ($workCards as $workCard) {
                        $currency = new WorkCard(['scenario' => 'create']);
                        $currency->work_id = $this->id;
                        $currency->card_id = $workCard->id;
                        $currency->text = isset($workCard->message)?$workCard->message:null;
                        $currency->time = isset($workCard->workCardTime)?$workCard->workCardTime:null;
                        $currency->save();
                    }
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            WorkTheses::deleteAll('work_id=:work_id', [':work_id' => $this->id]);
            if ($this->inputWorkTheses) {

                if (is_array($this->inputWorkTheses)) {
                    foreach ($this->inputWorkTheses as $thesis) {
                        if (!empty($thesis['title'])) {
                            $model = new WorkTheses();
                            $model->title = $thesis['title'];
                            $model->work_id = $this->id;
                            $model->save();
                        }
                    }
                } else {
                    $WorkTheses = json_decode($this->inputWorkTheses);
                    foreach ($WorkTheses as $thesis) {
                        if ($thesis->title) {
                            $model = new WorkTheses();
                            $model->title = $thesis->title;
                            $model->work_id = $this->id;
                            $model->save();

                        }
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Событие до удаления модели
     * @return bool|void
     */
    public function beforeDelete()
    {
        WorkCard::deleteAll('work_id=:work_id', [':work_id' => $this->id]);
        WorkTheses::deleteAll('work_id=:work_id', [':work_id' => $this->id]);
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkCards()
    {
        return $this->hasMany(WorkCard::className(), ['work_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkTheses()
    {
        return $this->hasMany(WorkTheses::className(), ['work_id' => 'id']);
    }

    /**
     * Промежуточная табличка
     * @return mixed
     */
    public function getCards()
    {
        return $this->hasMany(Card::className(), ['id' => 'card_id'])
            ->via('workCards')->with('cardText');
    }

}
