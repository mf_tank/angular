<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\components\BaseARecord;

/**
 * Class Card
 * @package app\models
 */
class Card extends BaseARecord
{

    /**
     *  цели ученика
     */
    const TARGET_PUPIL = 1;

    /**
     * цели учителя
     */
    const TARGET_TEACHER = 2;

    /**
     * Массив целей
     * @var array
     */
    public static $TARGET = [
        self::TARGET_PUPIL => 'цели ученика',
        self::TARGET_TEACHER => 'цели учителя'
    ];

    const COLOR_RED = 1;
    const COLOR_BLUE = 2;

    /**
     * @var array
     */
    public static $COLOR_TYPE = [
        self::COLOR_RED => 'Красный',
        self::COLOR_BLUE => 'Синийы'
    ];


    const INDIVIDUAL_FIRST = 1;
    const INDIVIDUAL_GROUP = 2;

    /**
     * @var array
     */
    public static $INDIVIDUAL_TYPE = [
        self::INDIVIDUAL_FIRST => 'Индивидуальная',
        self::INDIVIDUAL_GROUP => 'Групповая'
    ];


    const USE_START = 1;
    const USE_CENTER = 2;
    const USE_FINISH = 3;

    /**
     * @var array
     */
    public static $USE_TYPE = [
        self::USE_START => 'Начало',
        self::USE_CENTER => 'Середина',
        self::USE_FINISH => 'Конец'
    ];


    /**
     * @var шаги
     */
    public $steps;

    /**
     * @var array
     */
    public $targetPupils;

    /**
     * @var array
     */
    public $targetTeachers;

    /**
     * @var string
     */
    public $familyFilter;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $workCardTime;

    /**
     * @var sting
     */
    public $sort;

    /**
     * @var string
     */
    public $sortLike;

    /**
     * @var string
     */
    public $sortUser;

    /**
     * @var array
     */
    public static $URL_PARAMS = [
        'energyUp' => ['Card' => ['color_type' => Card::COLOR_RED]],
        'energyDown' => ['Card' => ['color_type' => Card::COLOR_BLUE]],
        'start' => ['Card' => ['use_type' => Card::USE_START]],
        'center' => ['Card' => ['use_type' => Card::USE_CENTER]],
        'finish' => ['Card' => ['use_type' => Card::USE_FINISH]],
        'bigGroupsDown' => ['Card' => ['individual_type' => Card::INDIVIDUAL_FIRST]],
        'bigGroupsUp' => ['Card' => ['individual_type' => Card::INDIVIDUAL_GROUP]],
    ];

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), []);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['name'], 'unique', 'on' => ['create', 'update']],

            [['name'], 'string', 'max' => 255, 'on' => ['create', 'update']],
            [
                [
                    'name', 'lang',
                    'time',
                    'is_big_groups',
                    'color_type', 'individual_type',
                    'targetPupils', 'targetTeachers',
                    'use_type'
                ],
                'required',
                'on' => ['create', 'update']
            ],
            [
                [
                    'lang', 'time',
                    'is_big_groups',
                    'color_type', 'individual_type'
                ],
                'integer',
                'on' => ['create', 'update']
            ],

            [
                ['user_id'], 'exist', 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id'], 'on' => ['create', 'update']
            ],
            [
                ['family_id'], 'exist', 'targetClass' => Family::className(),
                'targetAttribute' => ['family_id' => 'id'], 'on' => ['create', 'update']
            ],

            [
                ['use_type'],
                'validateList', 'on' => ['create', 'update']
            ],

            [
                'user_id', 'default', 'value' => Yii::$app->getUser()->getId(), 'on' => ['create', 'update']
            ],

            [
                ['steps', 'targetPupils', 'targetTeachers'], 'safe', 'on' => ['create', 'update']
            ],

            [['id', 'name', 'lang'], 'safe', 'on' => ['search']],

            [[
                'sortLike',
                'sortUser',
                'sort',
                'color_type',
                'use_type',
                'is_big_groups',
                'targetPupils',
                'familyFilter',
                'targetTeachers',
                'individual_type'
            ], 'safe', 'on' => ['searchCard']]
        ]);
    }

    /**
     * Возвращаемые поля
     * @return array
     */
    public function fields()
    {
        return ArrayHelper::merge(
            [
                'cardText',
                'cardUser' => function($card) {
                    if(empty(Yii::$app->user->identity->id)) {
                        return null;
                    }
                    return $card->getCardUsers()
                        ->where(['user_id' => Yii::$app->user->identity->id])
                        ->one();
                },
                'message' => function($card) {
                    $workCard = $card->getCardWorks()
                        ->andWhere(['work_id' => Yii::$app->request->get('id')])
                        ->one();
                    if($workCard) {
                        return $workCard->text;
                    } else {
                        return null;
                    }
                },
                'workCardTime' => function($card) {
                    $workCard = $card->getCardWorks()
                        ->andWhere(['work_id' => Yii::$app->request->get('id')])
                        ->one();
                    if($workCard && $workCard->time) {
                        return $workCard->time;
                    } else {
                        return $card->time;
                    }
                },
            ],
            parent::fields()
        );
    }


    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'name' => Yii::t('app', 'Название'),
            'time' => Yii::t('app', 'Время на карточку(минуты)'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'family_id' => Yii::t('app', 'Семейство'),
            'targetPupils' => Yii::t('app', 'Цель ученика'),
            'targetTeachers' => Yii::t('app', 'Цель учителя'),

            'is_big_groups' => Yii::t('app', 'Подходит для больших групп?'),

            'color_type' => Yii::t('app', 'Цвет фона?'),
            'individual_type' => Yii::t('app', 'Группа?'),

            'use_type' => Yii::t('app', 'Где можно использовать'),
            'text' => Yii::t('app', 'Пример')
        ]);
    }

    /**
     * Функция валидации для списков
     * @param $attribute
     * @param $params
     */
    public function validateList($attribute, $params)
    {
        if(!empty($this->{$attribute}) && is_array($this->{$attribute})) {
            $this->{$attribute} = implode(' ', $this->{$attribute});
        }
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lang' => $this->lang,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }


    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchCard($param)
    {
        $params = [];
        if (!array_key_exists($param, self::$URL_PARAMS)) {
            if (stristr($param, 'family-')) {
                $params = [
                    'Card' => [
                        'familyFilter' => substr($param, 7)
                    ]
                ];
            } else if (stristr($param, 'targetPupil-')) {
                $params = [
                    'Card' => [
                        'targetPupils' => substr($param, 12)
                    ]
                ];
            } else if (stristr($param, 'targetTeacher-')) {
                $params = [
                    'Card' => [
                        'targetTeachers' => substr($param, 14)
                    ]
                ];
            } else {
                return [];
            }
        } else {
            $params = self::$URL_PARAMS[$param];
        }


        $query = self::find();
        $this->setScenario('searchCard');
        if (!($this->load($params) && $this->validate())) {
            return [];
        }

        $query->andFilterWhere([
            Card::getNameTableNoPattern().'.lang' => $this->lang,
            Card::getNameTableNoPattern().'.color_type' => $this->color_type,
            Card::getNameTableNoPattern().'.individual_type' => $this->individual_type
        ]);


        if(!empty($this->use_type)) {
            $likes = ['or'];
            $likes[] = [
                'like',
                Card::getNameTableNoPattern().'.use_type',
                $this->use_type
            ];
            $query->andFilterWhere($likes);
        }


        if(!empty($this->familyFilter)) {
            $query->innerJoinWith([
                'families' => function ($query) {
                    $query->andWhere(['=', Family::getNameTableNoPattern().'.name', $this->familyFilter]);
                },
            ]);
        }

        if(!empty($this->targetPupils)) {
            $query->innerJoinWith([
                'targets' => function ($query) {
                    $query->andWhere(['=',  Target::getNameTableNoPattern().'.name', $this->targetPupils]);
                },
            ]);
        }

        if(!empty($this->targetTeachers)) {
            $query->innerJoinWith([
                'targets' => function ($query) {
                    $query->andWhere(['=',  Target::getNameTableNoPattern().'.name', $this->targetTeachers]);
                },
            ]);
        }

        return $query->all();
    }


    /**
     * @param $params
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchCardFilter($params)
    {
        $query = self::find();
        $this->setScenario('searchCard');
        if (!($this->load($params) && $this->validate())) {
            return $query->all();
        }

        $query->andFilterWhere([
            Card::getNameTableNoPattern().'.lang' => $this->lang,
            Card::getNameTableNoPattern().'.color_type' => $this->color_type,
            Card::getNameTableNoPattern().'.is_big_groups' => $this->is_big_groups,
            Card::getNameTableNoPattern().'.individual_type' => $this->individual_type
        ]);


        if(!empty($this->use_type)) {
            $this->use_type = explode(',', $this->use_type);
            $likes = ['or'];
            foreach ($this->use_type as $type) {
                $likes[] = [
                    'like',
                    Card::getNameTableNoPattern().'.use_type',
                    $type
                ];
            }
            $query->andFilterWhere($likes);
        }


        if(!empty($this->targetPupils)) {
            $query->innerJoinWith([
                'targets' => function ($query) {
                    $query->andWhere(['IN',  Target::getNameTableNoPattern().'.id', $this->targetPupils]);
                },
            ]);
        }

        if(!empty($this->targetTeachers)) {
            $query->innerJoinWith([
                'targets' => function ($query) {
                    $query->andWhere(['IN',  Target::getNameTableNoPattern().'.id', $this->targetTeachers]);
                },
            ]);
        }


        if(!empty($this->sortLike)) {
            $query->joinWith([
                'cardUsers' => function ($query) {
                    $query->andWhere([
                        '=',
                        CardUser::getNameTableNoPattern().'.user_id',
                        Yii::$app->user->identity->id
                    ])->orderBy(CardUser::getNameTableNoPattern().'.id asc');
                },
            ], false);
        }

        if(!empty($this->sortUser)) {
            if($this->sortUser == 2) {
                $query->andWhere([
                    '=',
                    Card::getNameTableNoPattern().'.user_id',
                    Yii::$app->user->identity->id
                ]);
            }
        }


        if(!empty($this->sort)) {
            if($this->sort == 'name') {
                $query->orderBy(Card::getNameTableNoPattern().'.name');
            } else if($this->sort == 'time') {
                $query->orderBy(Card::getNameTableNoPattern().'.time asc');
            }
        }

        return $query->all();
    }

    /**
     * Событие после сохранения модели
     * @return bool|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        CardText::deleteAll('card_id=:card_id', [':card_id' => $this->id]);

        if(!is_array($this->targetPupils)) {
            $this->targetPupils = explode(',', $this->targetPupils);
        }
        if(!is_array($this->targetTeachers)) {
            $this->targetTeachers = explode(',', $this->targetTeachers);
        }

        if(!is_array($this->steps)) {
            $this->steps = explode(',', $this->steps);
            foreach ($this->steps as $step) {
                $currency = new CardText(['scenario' => 'create']);
                $currency->card_id = $this->id;
                $currency->text = $step;
                $currency->save();
            }
        } else {
            foreach ($this->steps as $step) {
                $currency = new CardText(['scenario' => 'create']);
                $currency->card_id = $this->id;
                $currency->popup_id = $step['popup_id'];
                $currency->text = $step['text'];
                $currency->save();
            }
        }


        CardTarget::deleteAll('card_id=:card_id', [':card_id' => $this->id]);
        foreach ($this->targetPupils as $target) {
            $currency = new CardTarget(['scenario' => 'create']);
            $currency->card_id = $this->id;
            $currency->target_id = $target;
            $currency->save();
        }
        foreach ($this->targetTeachers as $target) {
            $currency = new CardTarget(['scenario' => 'create']);
            $currency->card_id = $this->id;
            $currency->target_id = $target;
            $currency->save();
        }


        return parent::afterSave($insert, $changedAttributes);
    }


    /**
     * Событие до удаления модели
     * @return bool|void
     */
    public function beforeDelete()
    {
        CardText::deleteAll('card_id=:card_id', [':card_id' => $this->id]);
        CardTarget::deleteAll('card_id=:card_id', [':card_id' => $this->id]);
        CardUser::deleteAll('card_id=:card_id', [':card_id' => $this->id]);
        WorkCard::deleteAll('card_id=:card_id', [':card_id' => $this->id]);
        return parent::beforeDelete();
    }

    /**
     * Пользователь
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Семейство
     * @return \yii\db\ActiveQuery
     */
    public function getFamilies()
    {
        return $this->hasMany(Family::className(), ['id' => 'family_id']);
    }

    /**
     * Промежуточная табличка
     * @return mixed
     */
    public function getCardText()
    {
        return $this->hasMany(CardText::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardTargets()
    {
        return $this->hasMany(CardTarget::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardWorks()
    {
        return $this->hasMany(WorkCard::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardUsers()
    {
        return $this->hasMany(CardUser::className(), ['card_id' => 'id']);
    }

    /**
     * Промежуточная табличка
     * @return mixed
     */
    public function getTargets($target = false)
    {
        if(!$target) {
            return $this->hasMany(Target::className(), ['id' => 'target_id'])
                ->via('cardTargets');
        } else {
            return $this->hasMany(Target::className(), ['id' => 'target_id'])
                ->where(['is_target' => $target])
                ->via('cardTargets');
        }

    }

    /**
     * @param int $target
     * @return array
     */
    public function getArrayTargets($target = self::TARGET_PUPIL)
    {
        return ArrayHelper::getColumn(
            $this->getTargets()
                ->where(['is_target' => $target])
                ->asArray()->all(),
            'id'
        );
    }

    /**
     * Карточки
     * @param $lang
     * @return array
     */
    public static function getArrayCards($lang)
    {
        return ArrayHelper::map(
            self::find()->where([
                'lang' => $lang
            ])->asArray()->all(),
            'id',
            'name'
        );
    }


}
