<?php
/**
 * Письмо при успешной регистрации
 * @var $name  string имя пользователя
 * @var $hash  string хеш
 */

$this->title = 'Регистрация';

use yii\helpers\Url;
?>

Здравствуйте <?echo $name;?> Вы успешно зарегистрировались.


Пройдите по ссылке чтобы поддтвердить email
<a href="<?echo Url::toRoute(['/site/confirm', 'hash' => $hash], true);?>">
    ссылка
</a>



