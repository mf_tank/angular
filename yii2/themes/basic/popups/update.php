<?php
/**
 * Табы языков
 * @var $this \yii\web\View
 */

use yii\bootstrap\Tabs;
use app\components\BaseARecord;

$this->title = Yii::t('app', 'Подсказки');

echo Tabs::widget([
    'items' => [
        [
            'label' => BaseARecord::$LANG[$model->lang],
            'content' => $this->render('_form', ['model' => $model, 'lang' => $model->lang]),
            'active' => true
        ]
    ],
]);
