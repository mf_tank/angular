<?php
/**
 * Грид
 * @var $this \yii\web\View
 * @var $dataProvider object DataProvider
 */

use yii\grid\GridView;
use yii\bootstrap\Nav;
use kartik\select2\Select2;
use app\components\BaseARecord;

$this->title = Yii::t('app', 'Подсказки');

$getValue = Yii::$app->request->get('Popup');

echo Nav::widget(
    [
        'items' => [
            [
                'label' => Yii::t('app', 'Создать'),
                'url' => ['popups/create'],
                'linkOptions' => [],
            ]
        ],
        'options' => ['class' =>'nav-pills']
    ]
);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'name',
        'text',
        [
            'attribute'=>'lang',
            'label' => 'Язык',
            'format' => 'raw',
            'value' => function($data) {
                return BaseARecord::$LANG[$data->lang];
            },
            'filter' => Select2::widget([
                'name' => 'Target[lang]',
                'data' => BaseARecord::$LANG,
                'value' => !empty($getValue['lang']) ? $getValue['lang'] : '',
                'language' => 'ru',
                'options' => ['placeholder' => Yii::t('app', 'Выберите язык')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}'
        ]
    ]
]);

