<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;

$this->title = 'Login';


$form = ActiveForm::begin(['layout' => 'default']);
echo $form->field($user, 'username');
echo $form->field($user, 'password')->passwordInput();
echo $form->field($user, 'rememberMe')->checkbox();

echo Button::widget([
    'label' => Yii::t('app', 'Войти'),
    'options' => ['class' => 'btn btn-primary']
]);
ActiveForm::end();
?>
