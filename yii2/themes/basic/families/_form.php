<?
use yii\bootstrap\Button;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'rubrics-form',
    'layout' => 'default',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => ['enctype' => 'multipart/form-data']
]);

echo $form->field($model, 'name');

$model->lang = $lang;
echo $form->field($model, 'lang')->hiddenInput()->label(false);

echo Button::widget([
    'label' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'),
    'options' => ['class' => 'btn btn-primary']
]);

ActiveForm::end();
?>