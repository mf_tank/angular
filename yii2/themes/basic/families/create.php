<?php
/**
 * Табы языков
 * @var $this \yii\web\View
 */

use yii\bootstrap\Tabs;
use app\components\BaseARecord;

$this->title = Yii::t('app', 'Семейство');

echo Tabs::widget([
    'items' => [
        [
            'label' => BaseARecord::$LANG[BaseARecord::LANG_RU],
            'content' => $this->render('_form', ['model' => $model, 'lang' => BaseARecord::LANG_RU]),
            'active' => true
        ],
        [
            'label' => BaseARecord::$LANG[BaseARecord::LANG_EN],
            'content' => $this->render('_form', ['model' => $model, 'lang' => BaseARecord::LANG_EN])
        ],
    ],
]);
