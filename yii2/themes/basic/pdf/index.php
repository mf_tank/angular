<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/styles.css">
</head>
<body>
<section class="az-task-wrap">
    <div class="az-subtitle">
        Название
    </div>
    <h1>
        <?echo $work->name;?>
    </h1>
    <hr>
    <div class="az-beginner">
        <span class="az-subtitle">
            Тезисы
        </span>
        <p class="az-comment">
            <?php
            foreach($work->workTheses as $thesis){
                print $thesis->title.'<br />';
            } ?>
        </p>
    </div>
    <div class="az-margin-top60">
        <span class="az-time">
            Продолжительность занятия: <?echo $work->time;?> мин
        </span>
    </div>
</section>

<?
$start = $work->begin_count;
$center = $work->center_count;
foreach ($resultCards as $resultCard) {?>
<section class="az-task-wrap style2">
    <div>
        <table class="az-row">
            <tr>
                <?for ($i=0; $i<6; $i++) {
                    if(!empty($resultCard[$i])) {
                        $workCard = $resultCard[$i];
                    ?>
                    <td>
                        <table class="az-sf-col-header">
                            <tr>
                                <td>
                                    <?
                                    if(!empty($time)) {
                                        echo date('H:i', $time);
                                        $time += ($workCard->time*60);
                                        echo '-'.date('H:i', $time);
                                    } else {
                                        echo $workCard->time.' мин';
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <table class="az-sf-col-name">
                            <tr>
                                <td>
                                    <?echo $workCard->card->name;?>
                                </td>
                            </tr>
                        </table>

                        <table class="az-sf-col-middle">
                            <tr>
                                <td>
                                    <?
                                    if($workCard->card->color_type == 1) {
                                        echo 'Энергия: Бодрит';
                                    } else {
                                        echo 'Энергия: Успокаивает';
                                    }

                                    ?>
                                </td>
                            </tr>
                        </table>

                        <table  class="az-sf-col-middle">
                            <tr>
                                <td>
                                    <div>
                                        <?echo $workCard->text;?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <?}?>
                <?}?>
            </tr>
        </table>
    </div>
</section>
<?}?>

</body>
</html>

