<?
/**
 * Форма
 * @var $this \yii\web\View
 */

use yii\bootstrap\Button;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use unclead\multipleinput\MultipleInput;
use app\models\Target;
use app\models\Family;
use app\models\Card;
use app\models\Popup;
?>

<div class="lang_form">
    <?
    $form = ActiveForm::begin([
        'id' => 'rubrics-form',
        'layout' => 'default',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => ['enctype' => 'multipart/form-data']
    ]);

    echo $form->field($model, 'name');

    $model->steps = $model->getCardText()->asArray()->all();
    echo $form->field($model, 'steps')->widget(MultipleInput::className(), [
        'options' => [
            'id' => 'steps_'.$lang
        ],
        'allowEmptyList'    => false,
        'columns' => [
            [
                'name'  => 'text',
                'type' => 'textarea',
                'title' => Yii::t('app', 'Шаг'),
            ],
            [
                'name'  => 'popup_id',
                'title' => Yii::t('app', 'Подсказка'),
                'type' => 'dropDownList',
                'items' => Popup::getArrayPopups($lang),
                'options' => [
                    'prompt' => Yii::t('app', 'Выберите подсказку')
                ],
            ]
        ]
    ])->label(false);

    echo $form->field($model, 'time');

    $model->targetPupils = $model->getArrayTargets();
    echo $form->field($model, 'targetPupils')->widget(Select2::classname(), [
        'data' => Target::getArrayTargets($lang),
        'language' => 'ru',
        'options' => [
            'id' => 'target_pupil_'.$lang,
            'placeholder' => Yii::t('app', 'Выберите цели ученика'),
            'multiple' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    $model->targetTeachers = $model->getArrayTargets(Card::TARGET_TEACHER);
    echo $form->field($model, 'targetTeachers')->widget(Select2::classname(), [
        'data' => Target::getArrayTargets($lang, Target::TARGET_TEACHER),
        'language' => 'ru',
        'options' => [
            'id' => 'target_teacher_'.$lang,
            'placeholder' => Yii::t('app', 'Выберите цели учителя'),
            'multiple' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);


    echo $form->field($model, 'family_id')->widget(Select2::classname(), [
        'data' => Family::getArrayFamilies($lang),
        'language' => 'ru',
        'options' => [
            'id' => 'family_'.$lang,
            'placeholder' => Yii::t('app', 'Выберите семейство')
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    echo $form->field($model, 'is_big_groups')->checkbox();

    echo $form->field($model, 'color_type')->widget(Select2::classname(), [
        'data' => Card::$COLOR_TYPE,
        'language' => 'ru',
        'options' => [
            'id' => 'color_type_'.$lang,
            'placeholder' => Yii::t('app', 'Выберите цвет фона')
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    echo $form->field($model, 'individual_type')->widget(Select2::classname(), [
        'data' => Card::$INDIVIDUAL_TYPE,
        'language' => 'ru',
        'options' => [
            'id' => 'individual_type_'.$lang,
            'placeholder' => Yii::t('app', 'Выберите группу')
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    if(!empty($model->use_type) && is_string($model->use_type)) {
        $model->use_type = explode(' ', $model->use_type);
    }
    echo $form->field($model, 'use_type')->widget(Select2::classname(), [
        'data' => Card::$USE_TYPE,
        'language' => 'ru',
        'options' => [
            'id' => 'use_type_'.$lang,
            'multiple' => true,
            'placeholder' => Yii::t('app', 'Выберите группу')
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    $model->lang = $lang;
    echo $form->field($model, 'lang')->hiddenInput()->label(false);

    echo Button::widget([
        'label' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'),
        'options' => ['class' => 'btn btn-primary']
    ]);

    ActiveForm::end();
    ?>
</div>

