<?
/**
 * Форма
 * @var $this \yii\web\View
 */

use yii\bootstrap\Button;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use unclead\multipleinput\MultipleInput;
use app\models\Card;
use app\models\Work;
?>

<div class="lang_form">
    <?
    $form = ActiveForm::begin([
        'id' => 'rubrics-form',
        'layout' => 'default',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => ['enctype' => 'multipart/form-data']
    ]);

    echo $form->field($model, 'name');

    echo $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => Work::$STATUS,
        'language' => 'ru',
        'options' => [
            'id' => 'status_'.$lang,
            'placeholder' => Yii::t('app', 'Выберите тип занятия')
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    $model->inputWorkTheses = $model->getWorkTheses()->orderBy('id ASC')->asArray()->all();
    echo $form->field($model, 'inputWorkTheses')->widget(MultipleInput::className(), [
        'options' => [
            'id' => 'workTheses_'.$lang
        ],
        'allowEmptyList'    => false,
        'columns' => [
            [
                'name'  => 'title',
                'type' => 'textarea',
                'title' => Yii::t('app', 'Тезис'),
            ],
            [
                'name'  => 'work_id',
                'type'=>'hiddenInput',
                'title' => Yii::t('app', 'Тезис'),
            ],

        ]
    ])->label(false);

    echo $form->field($model, 'begin_count');
    echo $form->field($model, 'center_count');
    echo $form->field($model, 'finish_count');

    $model->workCards = $model->getWorkCards()->orderBy('id ASC')->asArray()->all();
    echo $form->field($model, 'workCards')->widget(MultipleInput::className(), [
        'options' => [
            'id' => 'workCards_'.$lang
        ],
        'allowEmptyList'    => false,
        'columns' => [
            [
                'name'  => 'card_id',
                'title' => Yii::t('app', 'Карточка'),
                'type' => 'dropDownList',
                'items' => Card::getArrayCards($lang),
                'options' => [
                    'prompt' => Yii::t('app', 'Выберите карточку')
                ],
            ],
            [
                'name'  => 'text',
                'type' => 'textarea',
                'title' => Yii::t('app', 'Сообщение'),
            ],
            [
                'name'  => 'time',
                'title' => Yii::t('app', 'Время карточки'),
            ]
        ]
    ])->label(false);

    echo $form->field($model, 'time');

    $model->lang = $lang;
    echo $form->field($model, 'lang')->hiddenInput()->label(false);

    echo Button::widget([
        'label' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'),
        'options' => ['class' => 'btn btn-primary']
    ]);

    ActiveForm::end();
    ?>
</div>

