<?
/**
 * Форма
 * @var $this \yii\web\View
 */

use yii\bootstrap\Button;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use app\models\Target;
?>

<div class="lang_form">
    <?
    $form = ActiveForm::begin([
        'id' => 'rubrics-form',
        'layout' => 'default',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => ['enctype' => 'multipart/form-data']
    ]);

    echo $form->field($model, 'name');

    echo $form->field($model, 'is_target')->widget(Select2::classname(), [
        'data' => Target::$TARGET,
        'language' => 'ru',
        'options' => [
            'id' => 'target_'.$lang,
            'placeholder' => Yii::t('app', 'Выберите цель')
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    $model->lang = $lang;
    echo $form->field($model, 'lang')->hiddenInput()->label(false);

    echo Button::widget([
        'label' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'),
        'options' => ['class' => 'btn btn-primary']
    ]);

    ActiveForm::end();
    ?>
</div>

