<?php
/**
 * Грид
 * @var $this \yii\web\View
 * @var $dataProvider object DataProvider
 */

use yii\grid\GridView;
use yii\bootstrap\Nav;
use kartik\select2\Select2;
use app\components\BaseARecord;
use app\models\Target;

$this->title = Yii::t('app', 'Цели');

$getValue = Yii::$app->request->get('Target');

echo Nav::widget(
    [
        'items' => [
            [
                'label' => Yii::t('app', 'Создать'),
                'url' => ['targets/create'],
                'linkOptions' => [],
            ]
        ],
        'options' => ['class' =>'nav-pills']
    ]
);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'name',
        [
            'attribute'=>'lang',
            'label' => 'Цели',
            'format' => 'raw',
            'value' => function($data) {
                return Target::$TARGET[$data->is_target];
            },
            'filter' => Select2::widget([
                'name' => 'Target[is_target]',
                'data' => Target::$TARGET,
                'value' => !empty($getValue['is_target']) ? $getValue['is_target'] : '',
                'language' => 'ru',
                'options' => ['placeholder' => Yii::t('app', 'Выберите цель')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ],
        [
            'attribute'=>'lang',
            'label' => 'Язык',
            'format' => 'raw',
            'value' => function($data) {
                return BaseARecord::$LANG[$data->lang];
            },
            'filter' => Select2::widget([
                'name' => 'Target[lang]',
                'data' => BaseARecord::$LANG,
                'value' => !empty($getValue['lang']) ? $getValue['lang'] : '',
                'language' => 'ru',
                'options' => ['placeholder' => Yii::t('app', 'Выберите язык')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}'
        ]
    ]
]);

