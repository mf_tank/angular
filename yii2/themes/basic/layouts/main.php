<?php
/**
 * Шаблон админки
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags();?>
    <title>
        <?php echo Html::encode($this->title);?>
    </title>
    <?php $this->head();?>
</head>
<body>

<?php $this->beginBody();?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Yii::t('app', 'Сайт'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ]
            ]);
		if ( Yii::$app->user->isGuest ) {
            $items = [
                [
                    'label' => Yii::t('app', 'Вход'),
                    'url' => ['/']
                ]
            ];

			echo Nav::widget([
				'options' => ['class' => 'navbar-nav'],
				'items' => $items
			]);
		}
		else {
			$right_part = [
				[
					'label' => Yii::t('app', 'Выход'),
					'url' => ['site/logout'],
					'linkOptions' => ['data-method' => 'post']
				],
			];
			$left_part = [
                [
                    'label' => Yii::t('app', 'Пользователи'),
                    'url' => ['users/index']
                ],
                [
                    'label' => Yii::t('app', 'Карточки'),
                    'url' => ['cards/index']
                ],
                [
                    'label' => Yii::t('app', 'Занятия'),
                    'url' => ['works/index']
                ],
                [
                    'label' => Yii::t('app', 'Семейство'),
                    'url' => ['families/index']
                ],
                [
                    'label' => Yii::t('app', 'Цели'),
                    'url' => ['targets/index']
                ],
                [
                    'label' => Yii::t('app', 'Подсказки'),
                    'url' => ['popups/index']
                ]
            ];

			echo Nav::widget([
				'options' => ['class' => 'navbar-nav'],
				'items' => $left_part
			]);
			echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-right'],
				'items' => $right_part
			]);
		}

            NavBar::end();
        ?>

        <div class="container">
            <?echo $this->render('_flashes');?>

            <?php echo $content ?>
        </div>
    </div>

<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>

