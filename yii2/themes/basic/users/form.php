<?php
/**
 * Форма пользователей
 * @var $this \yii\web\View
 */

use yii\bootstrap\Tabs;

?>

<?php
echo Tabs::widget([
    'items' => [
        [
            'label' => 'Изменение данных пользователя',
            'content' => $this->render('change', ['model' => $model]),
            'active' => $is_active == 1 ? true : false,
        ],
        [
            'label' => 'Изменение пароля',
            'content' => $this->render('change_password', ['model' => $model]),
            'active' => $is_active == 2 ? true : false,
        ]
    ]
]);
?>
