<?php
/**
 * Изменение данных пользователя
 * @var $this \yii\web\View
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use yii\helpers\Html;

?>

<div style="margin: 20px 0 0 0;">

<?php $form = ActiveForm::begin([
    'id' => 'tags-form',
    'layout' => 'default',
    'action' => '/admin/users/changepassword/'.$model->id,
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?php echo $form->field($model, 'password_old')->passwordInput();?>

<?php echo $form->field($model, 'password')->passwordInput(['value' => '']);?>
<?php echo $form->field($model, 'password_repeat')->passwordInput();?>

<?php echo Button::widget([
    'label' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'),
    'options' => ['class' => 'btn btn-primary']
]);
?>
<?php ActiveForm::end();?>
</div>

