<?php
/**
 * Грид пользователей
 * @var $this \yii\web\View
 * @var $dataProvider object DataProvider
 * @var $searchModel object User
 */

use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\bootstrap\Alert;

$this->title = Yii::t('app', 'Пользователи');

echo Nav::widget(
    [
        'items' => [
            [
                'label' => Yii::t('app', 'Создать'),
                'url' => ['/users/create'],
                'linkOptions' => [],
            ]
        ],
        'options' => ['class' =>'nav-pills']
    ]
);

if(Yii::$app->session->getFlash('error')) {
    Alert::begin([
        'options' => [
            'class' => 'alert-info',
        ],
    ]);
    echo Yii::$app->session->getFlash('error');
    Alert::end();
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        [
            'attribute'=> 'username',
            'label' => Yii::t('app', 'id соцсети'),
            'content' => function($model) {
				$id = explode('_', $model->username);
				if(empty($id[1])) {
					return $model->username;
				} else {

					if($id[1] == 'vkontakte'){
						$base = 'https://vk.com/id';
					} else if($id[1] == 'odnoklassniki') {
						$base = 'https://ok.ru/profile/';
					} else {
						$base = 'https://facebook.com/';
					}
					return '<a href="'.$base . $id[0].'">'.$model->username.'</a>';
				}
            }
        ],
	    [
		    'attribute' => 'email_soc',
		    'label' => Yii::t('app', 'email соцсети'),
	    ],
	    [
		    'attribute' => 'email_subscribe',
		    'label' => Yii::t('app', 'email подписка'),
	    ],
        [
            'attribute'=>'img',
            'content' => function($model) {
                return \yii\helpers\Html::img($model->img);
            }
        ],
        [
          'attribute' => 'cards_count'
        ],
        [
            'attribute' => 'cards_like_count'
        ],
        [
            'attribute' => 'works_count'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}'
        ]
    ]
]);

