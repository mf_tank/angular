<?php
/**
 * Грид пользователей
 * @var $this \yii\web\View
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;
use app\models\User;

?>

<?php $form = ActiveForm::begin([
    'id' => 'tags-form',
    'layout' => 'default',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

<?php echo $form->field($model, 'username');?>

<?php echo Html::activeLabel($model, 'roles');?>
<div style="margin: 0 0 20px 0;">
    <?php echo Select2::widget([
        'name' => 'User[roles]',
        'data' => User::$ROLES_ALL,
        'language' => 'ru',
        'value' => $model->getRoles(),
        'options' => ['multiple' => true, 'placeholder' => Yii::t('app', 'Выберите Роль')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
</div>

<?php echo $form->field($model, 'name');?>
<?php echo $form->field($model, 'password')->passwordInput();?>
<?php echo $form->field($model, 'password_repeat')->passwordInput();?>


<?php echo Button::widget([
    'label' => $model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'),
    'options' => ['class' => 'btn btn-primary']
]);
?>
<?php ActiveForm::end();?>
