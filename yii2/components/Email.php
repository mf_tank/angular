<?php
/**
 * Компонент отправки на email
 */


namespace app\components;

use Yii;
use yii\base\Object;

/**
 * Компонент отсылки на почту
 * Class Email
 * @package app\components
 */
class Email extends Object
{

    /**
     * Отправка на почту
     * @param $message array
     * @param string $file путь к файлу
     * @return bool
     */
    public function send($message, $file = '')
    {
        try {
            $mail = Yii::$app->mailer->compose(
                $message['view'],
                $message['params']
            );

            $mail->setFrom($message['fromEmail'])
                ->setTo($message['email'])
                ->setSubject($message['subject']);
            if($file) {
                $mail->attach($file);
            }
            $mail->send();
            return true;
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }
    }


}
