<?php

namespace app\components;

use Yii;
use yii\db\ActiveRecord;
use yii\base\ErrorException;
use yii\base\Behavior;


/**
 * Class ManyToManyBehavior
 * @package app\components
 */
class ManyToManyBehavior extends Behavior
{
    /**
     * Relations list
     * @var array
     */
    public $relations = [];

    /**
     * Columns
     * @var array
     */
    public $columns = [];


    /**
     * Какие аттрибуты надо обновлять
     * @var array
     */
    private $updateAttribute = [];

    /**
     * Events list
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeRelations',
            ActiveRecord::EVENT_AFTER_INSERT => 'saveRelations',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveRelations',
        ];
    }

    /**
     * Событие до валидации
     * @return bool
     */
    public function beforeRelations($event)
    {
        $modelClass = get_class($event->sender);
        $post = Yii::$app->getRequest()->post($modelClass::getNameClass());
        foreach($this->relations as $attribute) {
            if (!empty($post[$attribute])) {
                $this->updateAttribute[] = $attribute;
            }
        }
        return true;
    }

    /**
     * Save relations value in data base
     * @param $event
     * @throws ErrorException
     * @throws \yii\db\Exception
     */
    public function saveRelations($event)
    {
        $model = $event->sender;
        $modelPk = $model->getPrimaryKey();

        foreach ($this->relations as $attributeName => $params) {
            if(array_search($attributeName, $this->updateAttribute) === false) {
                continue;
            }

            if (!$model->isAttributeSafe($attributeName)) {
                throw new ErrorException("Attribute \"{$attributeName}\" must be safe");
            }

            if (is_array($modelPk)) {
                throw new ErrorException("This behavior not supported composite primary key");
            }

            $relationName = $this->getRelationName($attributeName);
            $relation = $model->getRelation($relationName);

            if (empty($relation->via)) {
                throw new ErrorException("This attribute \"{$relationName}\" is not Many-to-Many relation");
            }


            if(!isset($relation->via->from) && isset($relation->via[0])) {
                $relationVia = $model->getRelation($relation->via[0]);
                $classNameVia = $relationVia->modelClass;
                $junctionTable = $classNameVia::tableName();
                list($relatedColumn) = array_values($relation->link);
                list($junctionColumn) = array_keys($relationVia->link);
                list($objectName) = array_values($relationVia->where);
            } else {
                list($junctionTable) = array_values($relation->via->from);
                list($relatedColumn) = array_values($relation->link);
                list($junctionColumn) = array_keys($relation->via->link);
            }

            $relationKeys  = $this->getNewValue($attributeName);

            if (!is_array($relationKeys)) {
                continue;
            }

            // Save relations data
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $connection = Yii::$app->db;

                // Remove relations
                if(!$model->isNewRecord) {
                    $connection->createCommand()
                        ->delete(
                            $junctionTable,
                            "{$junctionColumn} = :id", [':id' => $modelPk]
                        )->execute();
                }

                // Write new relations
                $junctionRows = [];
                foreach ($relationKeys as $key => $relatedPk) {
                    if(!is_string($relatedPk)) {
                        continue;
                    }



                    if(!empty($this->columns[$attributeName])) {
                        $columnAdd = [];
                        $junctionAdd = [];
                        $i=0;
                        foreach($this->columns[$attributeName] as $name) {
							$i++;
                            $columnAdd[$junctionColumn.'_'.$i] =  $name;
                            if(isset($model->{$attributeName.ucfirst($name)}[$key])) {
                                if($model->{$attributeName.ucfirst($name)}[$key] == '') {
                                    $junctionAdd[] = null;
                                } else {
                                    $junctionAdd[] = $model->{$attributeName.ucfirst($name)}[$key];
                                }
                            } else {
                                $junctionAdd[] = null;
                            }

                        }


                        $column = array_merge_recursive(
                            [$junctionColumn, $relatedColumn, 'created_at', 'updated_at'],
                            $columnAdd
                        );
                        $junctionRows[] = array_merge_recursive(
                            [$modelPk, $relatedPk, time(), time()],
                            $junctionAdd
                        );
                    } else {
                        if(!empty($relatedPk)) {
                            if (isset($objectName)) {
                                $column = [$junctionColumn, $relatedColumn, 'object_name', 'created_at', 'updated_at'];
                                $junctionRows[] = [$modelPk, $relatedPk, $objectName, time(), time()];
                            } else {
                                $column = [$junctionColumn, $relatedColumn, 'created_at', 'updated_at'];
                                $junctionRows[] = [$modelPk, $relatedPk, time(), time()];
                            }
                        }
                    }
                }

                if(!empty($junctionRows) && is_array($junctionRows)) {

                    $connection->createCommand()
                        ->batchInsert(
                            $junctionTable,
                            $column,
                            $junctionRows
                        )->execute();
                }

                $transaction->commit();
            } catch (\yii\db\Exception $ex) {
                $transaction->rollback();
            }
        }
    }

    /**
     * Get relation new value
     * @param $name
     * @return null
     */
    private function getNewValue($name)
    {
        if (isset($this->owner->{$name})) {
            return $this->owner->{$name};
        }
        return NULL;
    }

    /**
     * Get params relation
     * @param $attributeName
     * @return mixed
     * @throws ErrorException
     */
    private function getRelationParams($attributeName)
    {
        if (empty($this->relations[$attributeName])) {
            throw new ErrorException("Item \"{$attributeName}\" must be configured");
        }

        return $this->relations[$attributeName];
    }

    /**
     * Get source attribute name
     * @param $attributeName
     * @return null
     */
    private function getRelationName($attributeName)
    {
        $params = $this->getRelationParams($attributeName);

        if (is_string($params)) {
            return $params;
        } elseif (is_array($params) && !empty($params[0])) {
            return $params[0];
        }

        return NULL;
    }
}
