<?php

namespace app\components;


use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\web\UploadedFile;


/**
 * Class UploadFileManyBehavior
 * @package app\components
 */
class UploadFileManyBehavior extends Behavior
{

    /**
     * @var array $attributes
     */
    public $attributes;

    /**
     * @var string $attribute
     */
    public $attribute;

    /**
     * @var string имя класса ActiveRecord
     */
    public $modelName;

    /**
     * @var string $relationAttribute
     */
    public $relationAttribute;

    /**
     * @var string функция связи
     */
    public $relations;

    /**
     * @var array массив дополнительных элементов ActiveRecord
     */
    public $columns;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Функция инициализации настроек
     */
    public function init()
    {
        ini_set("max_execution_time", 3600);
    }

    /**
     * Подключение событий
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => ActiveRecord::EVENT_BEFORE_VALIDATE,
            ActiveRecord::EVENT_AFTER_INSERT => ActiveRecord::EVENT_AFTER_INSERT,
            ActiveRecord::EVENT_BEFORE_UPDATE => ActiveRecord::EVENT_BEFORE_UPDATE,
            ActiveRecord::EVENT_BEFORE_DELETE => ActiveRecord::EVENT_BEFORE_DELETE
        ];
    }

    /**
     * Событие до валидации
     * @return bool
     */
    public function beforeValidate($event)
    {
        foreach($this->attributes as $attribute) {
            $this->owner->{$attribute} = UploadedFile::getInstances($this->owner, $attribute);
        }
        return true;
    }

    /**
     * Событие до вставки модели
     * @param $event
     * @return bool
     */
    public function afterInsert($event)
    {
        foreach($this->attributes as $attribute) {
            foreach($this->owner->{$attribute} as $fileUpload) {
                if ($fileUpload instanceof UploadedFile) {
                    $this->saveFileOnAttribute($fileUpload);
                }
            }
        }
        return true;
    }

    /**
     * Событие обновления
     * @param $event
     * @return bool
     */
    public function beforeUpdate($event)
    {
        foreach($this->attributes as $attribute) {
            if(!empty($this->owner->{$attribute})) {
                foreach ($this->owner->{$attribute} as $fileUpload) {
                    if ($fileUpload instanceof UploadedFile) {
                        $this->saveFileOnAttribute($fileUpload);
                    }
                }
            }
        }
        return true;
    }


    /**
     * Событие до удаления модели
     * @param $event
     * @return bool
     */
    public function beforeDelete()
    {
        foreach($this->owner->{$this->relations} as $file) {
            $file->delete();
        }
        return true;
    }

    /**
     * Сохраняет файлы и присваивает путь аттрибутам модели
     * @param object $fileUpload экземпляр объекта UploadedFile
     */
    public function saveFileOnAttribute($fileUpload)
    {
        $model = new $this->modelName(['scenario' => 'create']);
        if($model instanceof ActiveRecord) {
            $className = $model->className();
            $path = $className::PATH_URL . $className::getNameTable();
            $file = $path . '/' . uniqid('', true) . '.' . $fileUpload->extension;
            if (!file_exists(Yii::getAlias('@app/web') . $path)) {
                mkdir(Yii::getAlias('@app/web') . $path, '0777', true);
                chmod(Yii::getAlias('@app/web') . $path, 0777);
            }
            $fileUpload->saveAs(Yii::getAlias('@app/web') . $file);
            if (!empty($this->relationAttribute)) {
                $model->{$this->relationAttribute} = $this->owner->getPrimaryKey();
            }
            foreach($this->columns as $key => $column) {
                $model->{$key} = $column;
            }
            $model->{$this->attribute} = $file;
            $model->detachBehavior('uploadFileBehavior');
            $model->save(false);
        }
    }
}
