<?php

namespace app\components;

use yii\web\Controller;
use yii\filters\AccessControl;


/**
 * Class BaseController
 * @package app\modules\admin\components
 */
abstract class BaseController extends Controller
{

    /**
     * Шаблон
     * @var string
     */
    public $layout = 'main';

    /**
     * Функция для поведений
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['admin']
                    ],
                ],
            ]
        ];
    }
}
