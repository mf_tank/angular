<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\Feedback;
use app\components\Email;

/**
 * Рассылка на email
 * Class EmailController
 * @package app\commands
 */
class EmailController extends Controller
{

    /**
     * Рассылка на почту
     */
    public function actionFeedback()
    {
        $feedbacks = Feedback::find()->where('is_email=:is_email', [
            ':is_email' => 0
        ])->all();
        foreach($feedbacks as $feedback) {
            $message = [
                'view' =>  'feedback',
                'params' => [
                    'name' => $feedback->name,
                    'phone' => $feedback->phone
                ],
                'fromEmail' => Yii::$app->params['fromEmail'],
                'email' => Yii::$app->params['adminEmail'],
                'subject' => Yii::t('app', 'Обратная связь')
            ];
            $email = new Email();
            $email->send($message);

            $feedback->is_email = 1;
            $feedback->save(false);
        }
    }
}
