<?php

namespace app\commands;

use app\models\Card;
use app\models\Work;
use Yii;
use yii\console\Controller;
use app\models\User;


/**
 * Class BaseController
 * @package app\commands
 */
class BaseController extends Controller
{

    /**
     *  Удаление карточек и занятий не админа
     */
    public function actionDeletecards()
    {
        $user = User::findByUsername('skugarev.andrey@gmail.com');

        $cards = Card::find()->all();
        foreach ($cards as $card) {
            if($card->user_id != $user->id) {
                $card->delete();
            }
        }

        $works = Work::find()->all();
        foreach ($works as $work) {
            if($work->user_id != $user->id) {
                $work->delete();
            }
        }

    }
}
