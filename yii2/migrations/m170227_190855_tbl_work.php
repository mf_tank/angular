<?php

use yii\db\Migration;

/**
 * Class m170227_190855_tbl_work
 */
class m170227_190855_tbl_work extends Migration
{
    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->createTable('{{%work}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'theses' => $this->text(),
            'time' => $this->integer()->notNull(),
            'begin_count' => $this->integer()->notNull()->defaultValue(1),
            'center_count' => $this->integer()->notNull()->defaultValue(1),
            'finish_count' => $this->integer()->notNull()->defaultValue(1),
            'status' => $this->boolean()->notNull()->defaultValue(1),
            'lang' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->addForeignKey('work_user', '{{%work}}', 'user_id', '{{%user}}', 'id');
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $this->dropTable('{{%work}}');
    }
}
