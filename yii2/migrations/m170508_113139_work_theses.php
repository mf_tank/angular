<?php

use yii\db\Migration;

class m170508_113139_work_theses extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%work_theses}}', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->notNull(),
            'work_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createIndex('idx-work_theses-work_id', '{{%work_theses}}', 'work_id');
        $this->addForeignKey('fk-work_theses-work_id', '{{%work_theses}}', 'work_id', '{{%work}}', 'id');
        $works = \app\models\Work::find()->all();

        $connection = Yii::$app->db;
        foreach ($works as $work) {
            if($work->theses) {
                $connection->createCommand()->insert('{{%work_theses}}', [
                    'title' => $work->theses,
                    'work_id' => $work->id,
                    'created_at' => $work->created_at,
                    'updated_at' => $work->updated_at,
                    'author_id' => $work->author_id,
                    'updater_id' => $work->updater_id])->execute();
            }

        }
        $this->dropColumn('{{%work}}', 'theses');
    }

    public function safeDown()
    {
        $this->addColumn('{{%work}}', 'theses', $this->text());
        $theses = (new \yii\db\Query())
            ->select(['title', 'work_id'])
            ->from('{{%work_theses}}')
            ->all();
        foreach ($theses as $thesis) {
            $work = \app\models\Work::find(['id' => $thesis['work_id']])->one();
            $work->theses .= $this['title'] . ';';
            $work->save();
        }

        $this->dropForeignKey('fk-work_theses-work_id', '{{%work_theses}}');
        $this->dropTable('{{%work_theses}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170508_113139_work_theses cannot be reverted.\n";

        return false;
    }
    */
}
