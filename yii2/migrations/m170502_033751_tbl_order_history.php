<?php

use yii\db\Migration;

class m170502_033751_tbl_order_history extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%order_history}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->createIndex('idx-order_history-order_id', '{{%order_history}}', 'order_id');
        $this->addForeignKey('fk-order_history-order_id', '{{%order_history}}', 'order_id', '{{%order}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-order_history-order_id', '{{%order_history}}');
        $this->dropTable('{{%order_history}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170502_033751_tbl_order_history cannot be reverted.\n";

        return false;
    }
    */
}
