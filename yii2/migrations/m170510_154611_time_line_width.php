<?php

use yii\db\Migration;

class m170510_154611_time_line_width extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%work}}', 'begin_count', $this->integer()->defaultValue(15));
        $this->alterColumn('{{%work}}', 'center_count', $this->integer()->defaultValue(70));
        $this->alterColumn('{{%work}}', 'finish_count', $this->integer()->defaultValue(15));
        \app\models\Work::updateAll(['begin_count' => 15, 'center_count'=>70, 'finish_count'=>15]);
    }

    public function safeDown()
    {
        $this->alterColumn('{{%work}}', 'begin_count', $this->integer()->defaultValue(1));
        $this->alterColumn('{{%work}}', 'center_count', $this->integer()->defaultValue(1));
        $this->alterColumn('{{%work}}', 'finish_count', $this->integer()->defaultValue(1));
        \app\models\Work::updateAll(['begin_count' => 1, 'center_count'=>1, 'finish_count'=>1]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170510_154611_time_line_width cannot be reverted.\n";

        return false;
    }
    */
}
