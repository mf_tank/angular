<?php

use yii\db\Migration;

class m170511_053555_user_subscribe_email extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'email_subscribe', $this->string());
        $this->addColumn('{{%user}}', 'after_register_popup', $this->boolean()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'email_subscribe');
        $this->dropColumn('{{%user}}', 'after_register_popup');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170511_053555_user_subscribe_email cannot be reverted.\n";

        return false;
    }
    */
}
