<?php

use yii\db\Migration;

/**
 * Class m170309_170303_tbl_card_target
 */
class m170309_170303_tbl_card_target extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->dropForeignKey('card_target_pupil', '{{%card}}');
        $this->dropForeignKey('card_target_teacher', '{{%card}}');

        $this->dropColumn('{{%card}}', 'target_pupil_id');
        $this->dropColumn('{{%card}}', 'target_teacher_id');
        $this->createTable('{{%card_target}}', [
            'id' => $this->primaryKey(),
            'card_id' => $this->integer()->notNull(),
            'target_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $this->addForeignKey('work_card_work', '{{%work_card}}', 'work_id', '{{%work}}', 'id');
        $this->addForeignKey('work_card_card', '{{%work_card}}', 'card_id', '{{%card}}', 'id');
        $this->createIndex('work_card2', '{{%work_card}}', 'work_id, card_id', true);

        $this->addColumn('{{%card}}', 'target_pupil_id', $this->integer());
        $this->addColumn('{{%card}}', 'target_teacher_id', $this->integer());
        $this->addForeignKey('card_target_pupil', '{{%card}}', 'target_pupil_id', '{{%target}}', 'id');
        $this->addForeignKey('card_target_teacher', '{{%card}}', 'target_teacher_id', '{{%target}}', 'id');
        $this->dropTable('{{%card_target}}');
    }
}
