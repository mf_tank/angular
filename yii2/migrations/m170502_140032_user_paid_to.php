<?php

use yii\db\Migration;

class m170502_140032_user_paid_to extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'paid_to', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'paid_to');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170502_140032_user_paid_to cannot be reverted.\n";

        return false;
    }
    */
}
