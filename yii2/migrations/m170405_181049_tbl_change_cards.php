<?php

use yii\db\Migration;

/**
 * Class m170405_181049_tbl_change_cards
 */
class m170405_181049_tbl_change_cards extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->dropColumn('{{%card}}', 'text');
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $this->addColumn('{{%card}}', 'text', $this->text()->notNull());
    }

}
