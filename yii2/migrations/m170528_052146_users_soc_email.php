<?php

use yii\db\Migration;

class m170528_052146_users_soc_email extends Migration
{
    public function safeUp()
    {
	    $this->addColumn('{{%user}}', 'email_soc', $this->string());
    }

    public function safeDown()
    {
	    $this->dropColumn('{{%user}}', 'email_soc');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170528_052146_users_soc_email cannot be reverted.\n";

        return false;
    }
    */
}
