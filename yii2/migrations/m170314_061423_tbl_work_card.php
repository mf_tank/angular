<?php

use yii\db\Migration;

/**
 * Class m170314_061423_tbl_work_card
 */
class m170314_061423_tbl_work_card extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->dropTable('{{%work}}');
        $this->createTable('{{%work}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'theses' => $this->text(),
            'time' => $this->integer()->notNull(),
            'lang' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->addForeignKey('work_user', '{{%work}}', 'user_id', '{{%user}}', 'id');


        $this->createTable('{{%work_card}}', [
            'id' => $this->primaryKey(),
            'work_id' => $this->integer()->notNull(),
            'card_id' => $this->integer()->notNull(),
            'text' => $this->text(),
            'time' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ]);
        $this->addForeignKey('work_card_work', '{{%work_card}}', 'work_id', '{{%work}}', 'id');
        $this->addForeignKey('work_card_card', '{{%work_card}}', 'card_id', '{{%card}}', 'id');
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $this->dropTable('{{%work_card}}');

        $this->dropForeignKey('work_user', '{{%work}}');
        $this->dropColumn('{{%work}}', 'user_id');
    }

}
