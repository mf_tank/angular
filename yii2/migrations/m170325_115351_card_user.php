<?php

use yii\db\Migration;

/**
 * Class m170325_115351_card_user
 */
class m170325_115351_card_user extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->createTable('{{%card_user}}', [
            'id' => $this->primaryKey(),
            'card_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ]);

        $this->addForeignKey('card_user_card', '{{%card_user}}', 'card_id', '{{%card}}', 'id');
        $this->addForeignKey('card_user_user', '{{%card_user}}', 'user_id', '{{%user}}', 'id');
        $this->createIndex('card_user2', '{{%card_user}}', 'user_id, card_id', true);
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $this->dropTable('{{%card_user}}');
    }
}
