<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\User;

/**
 * Class m150331_072245_tbl_user
 */
class m150331_072245_tbl_user extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING.' NOT NULL',
            'password' => Schema::TYPE_STRING.' NOT NULL',
            'auth_key' => Schema::TYPE_STRING.' NOT NULL',
            'access_token' => Schema::TYPE_STRING.' NOT NULL',
            'name' => Schema::TYPE_STRING,
            'img' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER.' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER.' NOT NULL',
            'author_id' => Schema::TYPE_INTEGER,
            'updater_id' => Schema::TYPE_INTEGER,
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->createIndex('user_username_unique', '{{%user}}', 'username', true);

        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $guest = $auth->createRole('guest');
        $auth->add($guest);

        $auth->addChild($admin, $guest);

        $model = new User(['scenario' => 'registration']);
        $model->username = 'skugarev.andrey@gmail.com';
        $model->password = '111';
        $model->password_repeat = '111';
        $model->author_id = 1;
        $model->updater_id = 1;
        if($model->save(false)) {
            $auth->assign(
                $auth->getRole('admin'),
                $model->getId()
            );
        }
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        $this->dropTable('{{%user}}');
    }
}
