<?php

use yii\db\Migration;

/**
 * Class m170225_104133_tbl_card
 */
class m170225_104133_tbl_card extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->createTable('{{%family}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'lang' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');


        $this->createTable('{{%target}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'lang' => $this->integer()->notNull()->defaultValue(1),
            'is_target' => $this->boolean()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');


        $this->createTable('{{%popup}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'lang' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');


        $this->createTable('{{%card}}', [
            'id' => $this->primaryKey(),

            'user_id' => $this->integer()->notNull(),
            'target_pupil_id' => $this->integer()->notNull(),
            'target_teacher_id' => $this->integer()->notNull(),
            'family_id' => $this->integer(),

            'name' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'time' => $this->integer()->notNull(),

            'is_featured' => $this->boolean()->notNull()->defaultValue(0),
            'is_big_groups' => $this->boolean()->notNull()->defaultValue(0),
            'is_pop' => $this->boolean()->notNull()->defaultValue(0),

            'color_type' => $this->boolean()->notNull()->defaultValue(1),
            'individual_type' => $this->boolean()->notNull()->defaultValue(1),

            'use_type' => $this->string()->notNull(),

            'lang' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('card_user', '{{%card}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('card_target_pupil', '{{%card}}', 'target_pupil_id', '{{%target}}', 'id');
        $this->addForeignKey('card_target_teacher', '{{%card}}', 'target_teacher_id', '{{%target}}', 'id');
        $this->addForeignKey('card_family', '{{%card}}', 'family_id', '{{%family}}', 'id');


        $this->createTable('{{%card_text}}', [
            'id' => $this->primaryKey(),
            'card_id' => $this->integer()->notNull(),
            'popup_id' => $this->integer(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('card_text_card', '{{%card_text}}', 'card_id', '{{%card}}', 'id');
        $this->addForeignKey('card_text_popup', '{{%card_text}}', 'popup_id', '{{%popup}}', 'id');
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $this->dropTable('{{%card_text}}');
        $this->dropTable('{{%card}}');
        $this->dropTable('{{%family}}');
        $this->dropTable('{{%target}}');
        $this->dropTable('{{%popup}}');
    }
}
