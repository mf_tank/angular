<?php
namespace app\modules\api\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Work;
use app\models\User;

/**
 * Class WorkRest
 * @package app\modules\api\models
 */
class WorkRest extends Work
{

    public $theses;
    /**
     * Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return '{{%work}}';
    }


    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'unique'],

            [['name'], 'string'],
            [['name', 'lang', 'time'], 'required'],

            [['lang', 'time'], 'integer'],

            [
                ['user_id'], 'exist', 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],

            [['user_id'], 'default', 'value' => Yii::$app->getUser()->identity->id],

            [
                ['workCards', 'workCardsText', 'workCardsTime', 'inputWorkTheses'], 'safe'
            ],

            [
                [
                    'begin_count', 'center_count', 'finish_count'
                ],
                'integer',
            ],

            [['theses'], 'safe'],
        ];
    }

    /**
     * Возвращаемые поля
     * @return array
     */
    public function fields()
    {
        return ArrayHelper::merge(
            [
                'cards',
                'theses' => function($model) {
                    $workTheses = $model->getWorkTheses()->orderBy('id ASC')->asArray()->all();

                    $result = [];
                    foreach ($workTheses as $item) {
                        $result[]['title'] = $item['title'];
                    }

                    return $result;
                },
            ],
            parent::fields()
        );
    }

}
