<?php
namespace app\modules\api\models;

use app\models\Target;

/**
 * Class TargetRest
 * @package app\modules\api\models
 */
class TargetRest extends Target
{

    /**
     * Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return '{{%target}}';
    }



}
