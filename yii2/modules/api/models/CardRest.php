<?php
namespace app\modules\api\models;

use Yii;
use app\models\Card;
use app\models\User;
use app\models\Family;
use app\components\BaseARecord;

/**
 * Class Card
 * @package app\models
 */
class CardRest extends Card
{
    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'unique'],

            [['name'], 'string', 'max' => 255],
            [
                [
                    'name',
                    'time',
                    'color_type',
                    'targetPupils', 'targetTeachers', 'individual_type',
                    'use_type'
                ],
                'required'
            ],
            [
                [
                    'lang', 'time',
                    'is_big_groups',
                    'color_type', 'individual_type'
                ],
                'integer'
            ],

            [
                ['user_id'], 'exist', 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
            [
                ['family_id'], 'exist', 'targetClass' => Family::className(),
                'targetAttribute' => ['family_id' => 'id']
            ],

            [
                ['use_type'],
                'validateList'
            ],

            [
                'user_id', 'default', 'value' => Yii::$app->user->identity->id
            ],
            [
                'lang', 'default', 'value' => BaseARecord::LANG_RU
            ],

            [
                ['steps', 'targetPupils', 'targetTeachers'], 'safe'
            ],
        ];
    }

    /**
     * Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return '{{%card}}';
    }



}
