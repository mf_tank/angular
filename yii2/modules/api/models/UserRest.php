<?php
namespace app\modules\api\models;

use Yii;
use app\models\User;
use app\components\BaseARecord;

/**
 * Class UserRest
 * @package app\modules\api\models
 */
class UserRest extends User
{
    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['id', 'name', 'img'], 'safe'
            ],
            ['email_subscribe', 'required', 'on'=>'email_subscribe'],
            ['email_subscribe', 'email', 'on'=>'email_subscribe'],
        ];
    }

    /**
     * Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'email_subscribe' => Yii::t('app', 'Email'),
        ]);
    }
}
