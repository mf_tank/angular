<?php
namespace app\modules\api\components;


use OAuth\Common\Exception\Exception;
use Yii;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\filters\auth\HttpBasicAuth;
use linslin\yii2\curl;
use app\models\User;
use Facebook;
use yii\web\Response;
use yii\db\Exception as DbException;
/**
 * Class RestController
 * @package app\modules\api\controllers
 */
class RestController extends ActiveController
{

    const VK = 'vkontakte';
    const FB = 'facebook';
    const OD = 'odnoklassniki';

    /**
     * Сеть
     * @var array
     */
    static $NET = [
        self::VK,
        self::FB,
        self::OD
    ];

    /**
     * @var bool
     */
    public $isDebug = false;


    /**
     * @return mixed
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
            ],
            'authenticator' => [
                'class' => HttpBasicAuth::className(),
            ],
            'contentNegotiator' => [
                'formats' => [
                    'json' => Response::FORMAT_JSON
                ]
            ]
        ], parent::behaviors());
    }


    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(Yii::$app->request->isGet || Yii::$app->request->isDelete) {
            $params = Yii::$app->request->getQueryParams();
        } else {
            $params = Yii::$app->request->bodyParams;
        }

        if(!empty($params['access_token']) && !empty($params['net'])) {
            $token = $params['access_token'];
            $token = substr((string)$token, 1);
            parse_str($token, $tokenParams);

            $userId = null;
            if(($key = array_search($params['net'], self::$NET)) !== false) {
                if(self::$NET[$key] == self::VK) {
                    $result = $this->vkProfile($tokenParams);
                } else if(self::$NET[$key] == self::FB) {
                    $result = $this->fbProfile($tokenParams);
                } else if(self::$NET[$key] == self::OD) {
                    $result = $this->odProfile($tokenParams);
                }
            }

            if (!empty($result)) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $user = User::findOne(['username' => $result['id']]);
                    if (empty($user)) {
                        $user = new User(['scenario' => 'socNetwork']);
                        $user->name = $result['name'];
                        $user->img = $result['img'];
                        $user->username = $result['id'];
                        $user->password = $result['id'];
                        $user->after_register_popup = 1;
	                    $user->email_soc = $result['email'];
                    } else {
                        $user->setScenario('socNetwork');
                        $user->name = $result['name'];
                        $user->img = $result['img'];
	                    $user->email_soc = $result['email'];
                    }
                    $user->save();

                    Yii::$app->user->login($user);
                    $transaction->commit();
                } catch (DbException $e) {
                    Yii::$app->user->login($user);
                }
            }
        }

        //$this->checkAccess($action, $model = null, $params = []);
        return parent::beforeAction($action);
    }

    /**
     * @param $params
     * @return array|null
     */
    public function vkProfile($params)
    {
        try {
            $curl = new curl\Curl();
            $response = $curl->setGetParams([
                'user_ids' => $params['user_id'],
                'fields' => 'photo_50,city,verified,email',
                'v' => '5.63'
            ])->get(
                'https://api.vk.com/method/users.get'
            );
            $result = Json::decode($response);

            return [
                'id' => $result['response'][0]['id'].'_'.self::VK,
                'name' => $result['response'][0]['first_name'],
                'img' => $result['response'][0]['photo_50'],
	            'email' => isset($result['response'][0]['email'])?$result['response'][0]['email']:null,
            ];
        }
        catch(Exception $e) {
                return null;
        }
    }

    /**
     * @param $params
     * @return null|string
     */
    public function fbProfile($params)
    {
        try {
            if(YII_ENV_DEV) {
                $fb = new Facebook\Facebook([
                    'app_id' => '162360127604727',
                    'app_secret' => '7364a160990a20cd0e0ee5434564ce16',
                    'default_graph_version' => 'v2.2',
                ]);
            } else {
                $fb = new Facebook\Facebook([
                    'app_id' => '801278483362957',
                    'app_secret' => '9e967064ac6c6334b113674df7abdf98',
                    'default_graph_version' => 'v2.2',
                ]);
            }


            $response = $fb->get('/me?fields=id,name,picture,email', $params['access_token']);
            $user = $response->getGraphUser();

            return [
                'id' => $user->getId().'_'.self::FB,
                'name' => $user->getName(),
                'img' => json_decode($user->getPicture(), true)['url'],
	            'email' => $user->getField('email'),
            ];

        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            return null;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            return null;
        }
    }

    /**
     * @param $params
     * @return null
     */
    public function odProfile($params)
    {
        try {
            $curl = new curl\Curl();
            if(YII_ENV_DEV) {
                $apiParams = Yii::$app->params['odnoklassniki'];
            } else {
                $apiParams = Yii::$app->params['odnoklassniki_boy'];
            }

            $response = $curl->setGetParams([
                'method' => 'users.getCurrentUser',
                'application_key' => $apiParams['clientPublic'],
                'sig' => md5(
                    'application_key=' . $apiParams['clientPublic'] .
                    'fields=name,PIC128X128,uid,emailformat=jsonmethod=users.getCurrentUser' .
                    $params['session_secret_key']
                ),
                'access_token' => $params['access_token'],
                'fields' => 'name,PIC128X128,uid,email',
                'format' => 'json'
            ])->get(
                'https://api.ok.ru/fb.do'
            );

            $response = json_decode($response, true);

            return [
                'id' => $response['uid'].'_'.self::OD,
                'name' => $response['name'],
                'img' => $response['pic128x128'],
	            'email' => isset($response['email'])?$response['email']:null,
            ];
        } catch(Exception $e) {
            return null;
        }
    }

    /**
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\ForbiddenHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        if (empty(Yii::$app->user->identity) && !Yii::$app->request->isOptions) {
            throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'Доступ запрещен'));
        }
    }
}
