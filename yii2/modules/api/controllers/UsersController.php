<?php
namespace app\modules\api\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\modules\api\components\RestController;
use app\modules\api\models\UserRest;
use app\models\User;
/**
 * Class UsersController
 * @package app\modules\api\controllers
 */
class UsersController extends RestController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\User';

    /**
     * @return mixed
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'email-subscribe', 'after-registration-popup'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login2']
                    ]
                ],
            ],
        ], parent::behaviors());
    }


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }

    /**
     * @return array
     */
    public function actionLogin()
    {
        $user = User::findOne(Yii::$app->user->identity->id);
        return ArrayHelper::toArray($user, [
            'app\models\User' => [
                'id',
                'name',
                'img',
                'after_register_popup',
                'email_subscribe'
            ],
        ]);
    }
    public function actionLogin2()
    {
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));
            try {
                if ($eauth->authenticate()) {
//					var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;
                    $identity = User::findByEAuth($eauth);
                    Yii::$app->getUser()->login($identity);
                    // special redirect with closing popup window
                    $eauth->redirect();
                }
                else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
                // close popup window and redirect to cancelUrl
//				$eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

    }
    public function actionEmailSubscribe()
    {
        $user = UserRest::findOne(Yii::$app->user->identity->id);
        $user->scenario = 'email_subscribe';
        $user->load(['email_subscribe'=>\Yii::$app->request->post('email_subscribe')], '');
        $user->after_register_popup = 0;
        if(!$user->save()){
            return $user;
        }
        return true;
    }

    public function actionAfterRegistrationPopup()
    {
        $user = UserRest::findOne(Yii::$app->user->identity->id);
        $user->after_register_popup = 0;
        $user->save();
        return true;
    }

}