<?php
namespace app\modules\api\controllers;


use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\Card;
use app\models\Work;
use app\modules\api\components\RestController;
use app\components\BaseARecord;
use yii\web\NotFoundHttpException;

/**
 * Class WorksController
 * @package app\modules\api\controllers
 */
class WorksController extends RestController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\WorkRest';


    /**
     * @return mixed
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['options', 'exampleall', 'example', 'cards'],
                        'roles' => ['@', '?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'index', 'view', 'update', 'delete'],
                        'roles' => ['@']
                    ],
                ],
            ]
        ], parent::behaviors());
    }


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index'], $actions['view']);

        return $actions;
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        $works = Work::find()->where('user_id=:user_id and lang=:lang and status=:status', [
            ':user_id' => Yii::$app->user->identity->id,
            ':lang' => BaseARecord::LANG_RU,
            ':status' => Work::STATUS_WORK
        ])->all();

        return ArrayHelper::toArray($works, [
            'app\models\Work' => [
                'id',
                'name',
                'time',
                'status',
                'created_at' => function($work) {
                    return date('d.m.Y', $work->created_at);
                }
            ]
        ]);
    }

    /**
     * @return array
     */
    public function actionView($id)
    {
        $work = Work::find()->where('id=:id and status=:status and lang=:lang', [
            ':id' => $id,
            ':status' => Work::STATUS_WORK,
            ':lang' => BaseARecord::LANG_RU,
           // ':user_id' => Yii::$app->user->identity->id,
        ])->one();

        if(!$work) {
            throw new NotFoundHttpException(Yii::t('app', '404'));
        }

        return ArrayHelper::toArray($work, [
            'app\models\Work' => [
                'id',
                'name',
                'time',
                'theses' => function($work) {
                    $workTheses = $work->getWorkTheses()->orderBy('id ASC')->asArray()->all();

                    $result = [];
                    foreach ($workTheses as $item) {
                        $result[]['title'] = $item['title'];
                    }

                    return $result;
                },
                'begin_count',
                'center_count',
                'finish_count',
                'status',
                'workCards' => function($work) {
                    return $work->getWorkCards()->orderBy('id ASC')->all();
                },
                'cards' => function($work) {
                    $workCards = $work->getWorkCards()->orderBy('id ASC')->all();
                    $result = [];
                    foreach ($workCards as $workCard) {
                        $result[] = $workCard->card;
                    }
                    return $result;
                },
                'created_at',
            ]
        ]);
    }

    /**
     * @return array
     */
    public function actionExampleall()
    {
        $works = Work::find()->where('status=:status and lang=:lang', [
            ':status' => Work::STATUS_VIEW,
            ':lang' => BaseARecord::LANG_RU,
        ])->all();

        return ArrayHelper::toArray($works, [
            'app\models\Work' => [
                'id',
                'name',
                'time',
                'theses' => function($work) {
                    $workTheses = $work->getWorkTheses()->orderBy('id ASC')->asArray()->all();

                    $result = [];
                    foreach ($workTheses as $item) {
                        $result[]['title'] = $item['title'];
                    }

                    return $result;
                },
                'begin_count',
                'center_count',
                'finish_count',
                'status',
                'workCards',
                'cards' => function($work) {
                    $workCards = $work->getWorkCards()->orderBy('id ASC')->all();
                    $result = [];
                    foreach ($workCards as $workCard) {
                        $result[] = $workCard->card;
                    }
                    return $result;
                },
                'created_at' => function($work) {
                    return date('d.m.Y', $work->created_at);
                }
            ]
        ]);
    }


    /**
     * @return array
     */
    public function actionExample($id)
    {
        $work = Work::find()->where('id=:id and lang=:lang', [
            ':id' => $id,
            ':lang' => BaseARecord::LANG_RU,
        ])->one();

        if(!$work) {
            throw new NotFoundHttpException(Yii::t('app', '404'));
        }

        return ArrayHelper::toArray($work, [
            'app\models\Work' => [
                'id',
                'name',
                'time',
                'theses' => function($work) {
                    $workTheses = $work->getWorkTheses()->orderBy('id ASC')->asArray()->all();

                    $result = [];
                    foreach ($workTheses as $item) {
                        $result[]['title'] = $item['title'];
                    }

                    return $result;
                },
                'begin_count',
                'center_count',
                'finish_count',
                'status',
                'workCards' => function($work) {
                    return $work->getWorkCards()->orderBy('id ASC')->all();
                },
                'cards' => function($work) {
                    $workCards = $work->getWorkCards()->orderBy('id ASC')->all();
                    $result = [];
                    foreach ($workCards as $workCard) {
                        $result[] = $workCard->card;
                    }
                    return $result;
                },
                'created_at',
            ]
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionCards($id)
    {
        $work = Work::findOne($id);
        $cards = $work->cards;

        $cardsAll = Card::find()->all();
        foreach ($cardsAll as $key => $card) {
            foreach ($cards as $item) {
                if($card->id == $item->id) {
                    unset($cardsAll[$key]);
                    break;
                }
            }
        }

        $cardsAll= array_values($cardsAll);
        return ArrayHelper::toArray($cardsAll, [
            'app\models\Card' => [
                'id',
                'name',
                'time',
                'user_id',
                'family_id',
                'cardText' => function($card) {
                    return $card->cardText;
                },
                'is_big_groups',
                'color_type',
                'individual_type',
                'use_type',
                'workCardTime' => function($card) {
                    $workCard = $card->getCardWorks()
                        ->andWhere(['work_id' => Yii::$app->request->get('id')])
                        ->one();
                    if($workCard && $workCard->time) {
                        return $workCard->time;
                    } else {
                        return $card->time;
                    }
                },
                'cardUser' => function($card) {
                    if(!empty(Yii::$app->user->identity->id)) {
                        return $card->getCardUsers()
                            ->where(['user_id' => Yii::$app->user->identity->id])
                            ->one();
                    } else {
                        return null;
                    }
                },
            ],
        ]);
    }

    /**
     * @param string $action
     * @param \app\modules\api\models\WorkRest $model
     * @param array $params
     * @throws \yii\web\ForbiddenHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action === 'update' || $action === 'delete') {
            if ($model->user_id !== \Yii::$app->user->id)
                throw new \yii\web\ForbiddenHttpException(sprintf('Доступ запрещен.', $action));
        }
    }
}
