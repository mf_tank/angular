<?php
namespace app\modules\api\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use app\modules\api\components\RestController;
use app\models\Target;

/**
 * Class TargetsController
 * @package app\modules\api\controllers
 */
class TargetsController extends RestController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\TargetRest';

    /**
     * @return mixed
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@', '?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['@']
                    ],
                ],
            ]
        ], parent::behaviors());
    }

    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index'], $actions['update'], $actions['delete'], $actions['options']);

        return $actions;
    }

    /**
     * Цели ученика или учителя
     * @return array
     */
    public function actionIndex()
    {
        $targets = Target::find()
            ->where(['is_target' => Yii::$app->request->getQueryParam('is_target')])->all();
        return ArrayHelper::toArray($targets, [
            'app\models\Target' => [
                'id',
                'name',
            ],
        ]);
    }

}