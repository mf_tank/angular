<?php


namespace app\modules\api\controllers;


use app\models\Order;
use app\models\OrderHistory;
use \yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\modules\api\components\RestController;
use yii\db\Exception;
use app\models\yandex\Settings;
use app\models\yandex\YaMoneyCommonHttpProtocol;
/**
 * Class OrderController
 * @package app\modules\api\controllers
 */
class OrderController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\models\Order';

    /**
     * @return mixed
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['check', 'avisio'],
                        'roles' => ['@', '?']
                    ],
                    [
                        'allow' => false,
                    ]
                ],
            ]
        ], parent::behaviors());
    }

    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['update'],
            $actions['delete'],
            $actions['create']
        );

        return $actions;
    }

    public function actionCreate()
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $model = new Order();
            $model->scenario = 'create';
            $model->amount = \Yii::$app->params['yandex_money']['amount'];
            $model->user_id = \Yii::$app->getUser()->getId();
            $model->save();
            $history = new OrderHistory();
            $history->status = OrderHistory::STATUS_CREATED;
            $history->order_id = $model->id;
            $model->link('history', $history);

            $transaction->commit();

            return [
                'params' => [
                    'sum' => $model->formattedAmount,
                    'orderNumber' => $model->id,
                    'customerNumber' => $model->user_id,
                    'shopId' => \Yii::$app->params['yandex_money']['shopId'],
                    'scid' => \Yii::$app->params['yandex_money']['scid'],
                ],
                'action' => \Yii::$app->params['yandex_money']['demo'] ? 'https://demomoney.yandex.ru/eshop.xml' : 'https://money.yandex.ru/eshop.xml',
            ];

        } catch (Exception $e) {
            $transaction->rollBack();
            return $model->getErrors();
        }
    }

    public function actionCheck()
    {
        $order = Order::findOne(intval($_REQUEST['orderNumber']));
        $settings = new Settings();
        $yaMoneyCommonHttpProtocol = new YaMoneyCommonHttpProtocol("checkOrder", $settings, $order);
        $yaMoneyCommonHttpProtocol->processRequest($_REQUEST);
        exit;
    }

    public function actionAvisio()
    {
        $order = Order::findOne(intval($_REQUEST['orderNumber']));
        $settings = new Settings();
        $yaMoneyCommonHttpProtocol = new YaMoneyCommonHttpProtocol("paymentAviso", $settings, $order);
        $yaMoneyCommonHttpProtocol->processRequest($_REQUEST);
        exit;
    }
}