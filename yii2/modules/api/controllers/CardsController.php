<?php
namespace app\modules\api\controllers;


use Yii;
use yii\filters\AccessControl;
use \yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\components\RestController;
use app\components\BaseARecord;
use app\models\Card;
use app\models\Family;
use app\models\Target;
use app\models\CardUser;
use app\models\Work;



/**
 * Class CardsController
 * @package app\controllers
 */
class CardsController extends RestController
{
    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\CardRest';

    /**
     * @return mixed
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['filter', 'params', 'filtercards', 'view', 'like'],
                        'roles' => ['@', '?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['like', 'create'],
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'like'   => ['put', 'options']
                ],
            ]
        ], parent::behaviors());
    }


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['update'],
            $actions['delete']
        );

        return $actions;
    }

    /**
     * @return mixed
     */
    public function actionFilter()
    {
        $families = Family::find()->where(['lang' => BaseARecord::LANG_RU])->all();
        $familyCardsCount = [];
        foreach ($families as $family) {
            $familyCardsCount[] = [
                'id' => $family->name,
                'name' => $family->name,
                'count' => Card::find()->where([
                    'family_id' => $family->id,
                    'lang' => BaseARecord::LANG_RU
                ])->count()
            ];
        }

        $targets = Target::findAll(['is_target' => Target::TARGET_PUPIL]);
        $targetsCardsCount = [];
        foreach ($targets as $target) {
            $targetsCardsCount[] = [
                'id' => $target->id,
                'name' => $target->name,
                'count' => $target->getCards()->where(['lang' => BaseARecord::LANG_RU])->count()
            ];
        }


        $targets = Target::findAll(['is_target' => Target::TARGET_TEACHER]);
        $targetsCardsCountTeachers = [];
        foreach ($targets as $target) {
            $targetsCardsCountTeachers[] = [
                'id' => $target->id,
                'name' => $target->name,
                'count' => $target->getCards()->where(['lang' => BaseARecord::LANG_RU])->count()
            ];
        }

        $result = [
            'energyUp' => Card::find()->where([
                'color_type' => Card::COLOR_RED,
                'lang' => BaseARecord::LANG_RU
            ])->count(),
            'energyDown' => Card::find()->where([
                'color_type' => Card::COLOR_BLUE,
                'lang' => BaseARecord::LANG_RU
            ])->count(),

            'start' => Card::find()->where([
                'use_type' => Card::USE_START,
                'lang' => BaseARecord::LANG_RU
            ])->count(),
            'center' => Card::find()->where([
                'use_type' => Card::USE_CENTER,
                'lang' => BaseARecord::LANG_RU
            ])->count(),
            'finish' => Card::find()->where([
                'use_type' => Card::USE_FINISH,
                'lang' => BaseARecord::LANG_RU
            ])->count(),

            'family' => $familyCardsCount,

            'targetPupils' => $targetsCardsCount,

            'targetTeachers' => $targetsCardsCountTeachers,

            'bigGroupsDown' => Card::find()->where([
                'individual_type' => Card::INDIVIDUAL_FIRST,
                'lang' => BaseARecord::LANG_RU
            ])->count(),

            'bigGroupsUp' => Card::find()->where([
                'individual_type' => Card::INDIVIDUAL_GROUP,
                'lang' => BaseARecord::LANG_RU
            ])->count(),
        ];

        return $result;
    }

    /**
     * Вывод количество карточек по фильтрам
     * @return array
     */
    public function actionParams($param)
    {
        $searchModel = new Card();
        $searchModel->lang = BaseARecord::LANG_RU;
        $cards = $searchModel->searchCard($param);

        return ArrayHelper::toArray($cards, [
            'app\models\Card' => [
                'id',
                'name',
                'time',
                'user_id',
                'family_id',
                'cardText' => function($card) {
                    return $card->cardText;
                },
                'targetPupils' => function($card) {
                    return $card->getTargets(Card::TARGET_PUPIL)->all();
                },
                'targetTeachers' => function($card) {
                    return $card->getTargets(Card::TARGET_TEACHER)->all();
                },
                'is_big_groups' => function($card) {
                    if($card->is_big_groups) {
                        return Yii::t('app', '> 20 человек');
                    } else {
                        return Yii::t('app', 'до 20 человек');
                    }
                },
                'color_type',
                'individual_type' => function($card) {
                    return Card::$INDIVIDUAL_TYPE[$card->individual_type];
                },
                'use_type' => function($card) {
                    $types = explode(' ', $card->use_type);
                    $result = [];
                    foreach ($types as $type) {
                        if(array_key_exists($type, Card::$USE_TYPE) !== false) {
                            $result[] = Card::$USE_TYPE[$type];
                        }
                    }
                    return $result;
                },
                'cardUser' => function($card) {
                    if(!empty(Yii::$app->user->identity->id)) {
                        return $card->getCardUsers()
                            ->where(['user_id' => Yii::$app->user->identity->id])
                            ->one();
                    } else {
                        return null;
                    }
                },
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actionFiltercards($id)
    {
        $searchModel = new Card();
        $searchModel->lang = BaseARecord::LANG_RU;
        $cardsAll = $searchModel->searchCardFilter(Yii::$app->request->queryParams);

        $work = Work::findOne($id);
        $cards = $work->cards;
/*
        foreach ($cardsAll as $key => $card) {
            foreach ($cards as $item) {
                if($card->id == $item->id) {
                    unset($cardsAll[$key]);
                    break;
                }
            }
        }
*/
        $cardsAll= array_values($cardsAll);

        return ArrayHelper::toArray($cardsAll, [
            'app\models\Card' => [
                'id',
                'name',
                'time',
                'user_id',
                'family_id',
                'time',
                'cardText' => function($card) {
                    return $card->cardText;
                },
                'targetPupils' => function($card) {
                    return $card->getTargets(Card::TARGET_PUPIL)->all();
                },
                'targetTeachers' => function($card) {
                    return $card->getTargets(Card::TARGET_TEACHER)->all();
                },
                'is_big_groups' => function($card) {
                    if($card->is_big_groups) {
                        return Yii::t('app', '> 20 человек');
                    } else {
                        return Yii::t('app', 'до 20 человек');
                    }
                },
                'color_type',
                'individual_type' => function($card) {
                    return Card::$INDIVIDUAL_TYPE[$card->individual_type];
                },
                'use_type' => function($card) {
                    $types = explode(' ', $card->use_type);
                    $result = [];
                    foreach ($types as $type) {
                        if(array_key_exists($type, Card::$USE_TYPE) !== false) {
                            $result[] = Card::$USE_TYPE[$type];
                        }
                    }
                    return $result;
                },
                'workCardTime' => function($card) {
                    $workCard = $card->getCardWorks()
                        ->andWhere(['work_id' => Yii::$app->request->get('id')])
                        ->one();
                    if($workCard && $workCard->time) {
                        return $workCard->time;
                    } else {
                        return $card->time;
                    }
                },
                'cardUser' => function($card) {
                    if(!empty(Yii::$app->user->identity->id)) {
                        return $card->getCardUsers()
                            ->where(['user_id' => Yii::$app->user->identity->id])
                            ->one();
                    } else {
                        return null;
                    }
                },
            ],
        ]);
    }


    /**
     * @param $id
     * @return array
     */
    public function actionView($id)
    {
        $card = Card::findOne($id);

        return ArrayHelper::toArray($card, [
            'app\models\Card' => [
                'id',
                'name',
                'time',
                'user_id',
                'user' => function($card) {
                    $user = $card->user;
                    return ArrayHelper::toArray($user, [
                        'app\models\User' => [
                            'id',
                            'username',
                            'name',
                            'updated_at',
                            'created_at'
                        ]
                    ]);
                },
                'family_id',
                'cardText' => function($card) {
                    return ArrayHelper::toArray($card->cardText, [
                        'app\models\CardText' => [
                                'author_id',
                                'updater_id',
                                'id',
                                'card_id',
                                'popup' => function($model) {
                                    return $model->popup;
                                },
                                'text',
                                'updated_at',
                                'created_at'
                            ]
                        ]);
                },
                'targetPupils' => function($card) {
                    return $card->getTargets(Card::TARGET_PUPIL)->all();
                },
                'targetTeachers' => function($card) {
                    return $card->getTargets(Card::TARGET_TEACHER)->all();
                },
                'is_big_groups' => function($card) {
                    if($card->is_big_groups) {
                        return Yii::t('app', 'Подкодит для больших групп');
                    }
                },
                'color_type',
                'color_type_name' => function($card) {
                    if($card->color_type == 1) {
                        return Yii::t('app', 'Бодрим');
                    } else {
                        return Yii::t('app', 'Успокаиваем');
                    }
                },
                'individual_type',
                'use_type' => function($card) {
                    $types = explode(' ', $card->use_type);
                    $result = [];
                    foreach ($types as $type) {
                        if(array_key_exists($type, Card::$USE_TYPE) !== false) {
                            $result[] = Card::$USE_TYPE[$type];
                        }
                    }
                    return $result;
                },
                'cardUser' => function($card) {
                    if(!empty(Yii::$app->user->identity->id)) {
                        return $card->getCardUsers()
                            ->where(['user_id' => Yii::$app->user->identity->id])
                            ->one();
                    } else {
                        return null;
                    }
                },
            ],
        ]);
    }


    /**
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionLike()
    {
        if(Yii::$app->request->isOptions) {
            return [];
        }

        if(empty(Yii::$app->user->identity->id)) {
            return [];
        }

        $params = Yii::$app->request->bodyParams;
        if(empty($params['card_id'])) {
            throw new \yii\web\HttpException(403, Yii::t('app', 'Неправильный запрос'));
        }


        $cardUser = CardUser::find()
            ->where([
                'user_id' => Yii::$app->user->identity->id,
                'card_id' => $params['card_id']
            ])
            ->one();
        if($cardUser) {
            $cardUser->delete();
        } else {
            $cardUser = new CardUser(['scenario' => 'create']);
            $cardUser->user_id = Yii::$app->user->identity->id;
            $cardUser->card_id = $params['card_id'];
            $cardUser->save();
        }

        return [];
    }

}