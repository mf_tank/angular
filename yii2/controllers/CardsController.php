<?php

namespace app\controllers;

use Yii;
use app\components\BaseController;
use app\models\Card;

/**
 * Class CardsController
 * @package app\controllers
 */
class CardsController extends BaseController
{

    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $searchModel = new Card();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Действие создание
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Card(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['cards/index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = Card::getModel($id);
        $model->setScenario('update');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['cards/index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Действие удаление
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = Card::getModel($id);
        if($model->delete()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Успешно удалено'));
        }
        $this->redirect(['cards/index']);
    }

}
