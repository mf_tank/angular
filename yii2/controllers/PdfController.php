<?php

namespace app\controllers;

use Yii;
use app\components\BaseController;
use app\models\Work;
use kartik\mpdf\Pdf;
use yii\filters\AccessControl;

/**
 * Class PdfController
 * @package app\controllers
 */
class PdfController extends BaseController
{

    /**
     * Функция для поведений
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?', '@']
                    ],
                ],
            ]
        ];
    }


    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionView($id, $time = '')
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->getHeaders()->set('Content-Type', 'application/pdf');


        $work = Work::getModel($id);
        $workCards = $work->getWorkCards()->orderBy('id ASC')->all();

        $resultCards = [];
        $items = [];
        foreach ($workCards as $key => $workCard) {
            $items[] = $workCard;
            if(((($key+1) % 6) == 0) || $key == count($workCards)-1) {
                $resultCards[] = $items;
                $items = [];
            }
        }

        $time = strtotime($time);
        $content = $this->renderPartial('/pdf/index.php', [
            'work' => $work,
            'resultCards' => $resultCards,
            'time' => $time
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@app/web/css/style.css',
            'options' => [
                'title' => 'Печать'
            ],
            'methods' => [
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        return $pdf->render();
    }


}
