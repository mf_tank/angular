<?php

namespace app\controllers;

use Yii;
use app\components\BaseController;
use app\models\Work;

/**
 * Class WorksController
 * @package app\controllers
 */
class WorksController extends BaseController
{

    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $searchModel = new Work();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }


    /**
     * Действие создание
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Work(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['works/index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = Work::getModel($id);
        $model->setScenario('update');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['works/index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Действие удаление
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = Work::getModel($id);
        if($model->delete()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Успешно удалено'));
        }
        $this->redirect(['works/index']);
    }

}
