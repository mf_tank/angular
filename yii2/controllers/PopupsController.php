<?php

namespace app\controllers;

use Yii;
use app\components\BaseController;
use app\models\Popup;

/**
 * Class PopupsController
 * @package app\controllers
 */
class PopupsController extends BaseController
{

    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $searchModel = new Popup();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Действие создание
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Popup(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['popups/index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = Popup::getModel($id);
        $model->setScenario('update');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['popups/index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Действие удаление
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = Popup::getModel($id);
        if($model->delete()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Успешно удалено'));
        }
        $this->redirect(['popups/index']);
    }

}
