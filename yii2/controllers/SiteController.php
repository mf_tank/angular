<?php

namespace app\controllers;

use app\models\Work;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\components\BaseController;
use app\models\User;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends BaseController
{

    /**
     * Функция для поведений
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'logout'],
                        'roles' => ['?', '@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => 'error'
            ],
        ];
    }


    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        if(!Yii::$app->getUser()->isGuest) {
            $this->redirect('/users/index');
        } else {
            $user = new User(['scenario' => 'login']);
            if ($user->load(Yii::$app->request->post()) && $user->login()) {
                $this->redirect(['/users/index']);
            } else {
                return $this->render('index', [
                    'user' => $user,
                ]);
            }
        }
    }

    /**
     * Разлогиниться
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
