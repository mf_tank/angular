<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\BaseController;
use app\models\User;

/**
 * Class UsersController
 * @package app\controllers
 */
class UsersController extends BaseController
{

    /**
     * Функция для поведений
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete', 'changepassword'],
                        'roles' => ['admin']
                    ]
                ],
            ]
        ];
    }

    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $searchModel = new User();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Действие создание
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['users/index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = User::getModel($id);
        $model->scenario = 'change';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['users/index']);
        }
        return $this->render('form', ['model' => $model, 'is_active' => 1]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionChangepassword($id)
    {
        $model = User::getModel($id);
        $model->scenario = 'change_password';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['users/index']);
        }
        return $this->render('form', ['model' => $model, 'is_active' => 2]);
    }

    /**
     * Действие удаление
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = User::getModel($id);
        if($id == Yii::$app->user->id) {
            Yii::$app->getSession()->setFlash(
                'error',
                Yii::t('app', 'Нельзя удалить себя самого')
            );
        } else {
            $model->delete();
        }
        $this->redirect(['users/index']);
    }
}
