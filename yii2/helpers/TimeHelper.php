<?php

namespace app\helpers;
use yii\helpers\BaseFileHelper;

/**
 * Class TimeHelper
 * @package app\helpers
 */
class TimeHelper extends BaseFileHelper
{

    /**
     * timestamp
     * @param $timestamp
     * @return bool
     */
    public static function isTimestamp($timestamp)
    {
        $check = (is_int($timestamp) OR is_float($timestamp))
            ? $timestamp
            : (string) (int) $timestamp;
        return  ($check === $timestamp)
        AND ( (int) $timestamp <=  PHP_INT_MAX)
        AND ( (int) $timestamp >= ~PHP_INT_MAX);
    }
}


