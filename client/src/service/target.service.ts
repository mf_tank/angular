import {Injectable}    from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BaseService} from "./base.service";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class TargetService extends BaseService {


    getTargetPupils() {
        this.params.set('is_target', '1');
        return this.http.get(this.apiUrl+'/targets', {search: this.params})
            .map(response => response.json())
            .catch((error: any) => {return Observable.throw(error);});
    }

    getTargetTeachers() {
        this.params.set('is_target', '2');
        return this.http.get(this.apiUrl+'/targets', {search: this.params})
            .map(response => response.json())
            .catch((error: any) => {return Observable.throw(error);});
    }
}
