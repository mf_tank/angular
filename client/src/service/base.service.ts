import { Injectable }    from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import {Headers, URLSearchParams} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { environment } from '../environments/environment';
declare var jQuery: any;

import 'rxjs/add/operator/toPromise';

@Injectable()
export class BaseService {

    protected headers = new Headers({'Content-Type': 'application/json'});
    protected apiUrl;


    protected params : URLSearchParams;

    constructor(
        protected http: Http,
        private router: Router,
    ) {
        this.apiUrl = environment.apiUrl;
        let hash = window.location.hash;
        if(hash) {
            Cookie.set('loginHash', hash, 0, '/');
        }
        this.params = new URLSearchParams();
        this.params.set('access_token', Cookie.get('loginHash'));
        this.params.set('lang', '1');
        this.params.set('net', Cookie.get('net'));
    }

    errors(error) {
        if(error.status == 403) {
            this.router.navigate(['']);
        } else if(error.status == 404) {
            this.router.navigate(['/404']);
        } else if(error.status == 500) {
            this.router.navigate(['/500']);
            jQuery('.main').remove();
            jQuery('section.az-500').removeClass('hidden');
        }
    }

}
