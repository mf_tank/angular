import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {BaseService} from "./base.service";


@Injectable()
export class OrderService extends BaseService {

    create() {
        return this.http.post(this.apiUrl+ '/order/create', this.params)
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }

}
