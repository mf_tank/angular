import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {Card} from "../model/card";
import {BaseService} from "./base.service";

@Injectable()
export class CardsService extends BaseService {

    getCards(param): Observable<any> {
        return this.http.get(this.apiUrl + '/cards/params/'+param, {search: this.params})
            .map(response => response.json())
            .catch((error: any) => {return Observable.throw(error);});
    }

    getFilterCards(id, params): Observable<any> {
        let key;
        let targetPupils = [];
        let targetTeachers = [];
        let steps = [];

        this.params.set('Card[color_type]', '');
        this.params.set('Card[use_type]', '');
        this.params.set('Card[targetPupils]', '');
        this.params.set('Card[targetTeachers]', '');
        this.params.set('Card[individual_type]', '');

        this.params.set('Card[sortLike]', '');
        this.params.set('Card[sort]', 'name');
        this.params.set('Card[sortUser]', '1');

        for(key in params) {
            if(params[key].name == 'use_type') {
                steps.push(params[key].value);
                this.params.set('Card['+params[key].name+']', steps.join(','));
            } else if(params[key].name == 'targetPupils') {
                targetPupils.push(params[key].value);
                this.params.set('Card['+params[key].name+']', targetPupils.join(','));
            } else if(params[key].name == 'targetTeachers') {
                targetTeachers.push(params[key].value);
                this.params.set('Card['+params[key].name+']', targetTeachers.join(','));
            } else {
                this.params.set('Card['+params[key].name+']', params[key].value);
            }
        }


        return this.http.get(this.apiUrl + '/cards/filtercards/'+id, {search: this.params})
            .map(response => response.json())
            .catch((error: any) => {return Observable.throw(error);});
    }

    getFilter(): Observable<any> {
        return this.http.get(this.apiUrl + '/cards/filter', {search: this.params})
            .map(response => response.json())
            .catch((error: any) => {return Observable.throw(error);});
    }


    getCard(id) {
        return this.http.get(this.apiUrl + '/cards/'+id, {search: this.params})
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }


    create(card: Card): Observable<any> {
        this.params.set('name', card.name);
        this.params.set('steps', card.steps.join(','));

        this.params.set('targetTeachers', card.targetTeachers.join(','));
        this.params.set('targetPupils',  card.targetPupils.join(','));

        this.params.set('use_type', card.use_type.join(','));
        this.params.set('color_type', card.color_type);
        this.params.set('individual_type', card.individual_type);

        this.params.set('is_big_groups', card.is_big_groups);
        this.params.set('time', card.time);


        return this.http.post(this.apiUrl+'/cards/create', this.params)
            .map(response => response.json())
            .catch((error: any) => {return Observable.throw(error);});
    }

    setLike(card: Card) {
        this.params.set('card_id', card.id);
        return this.http.put(this.apiUrl + '/cards/like', this.params)
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }
}
