import { Injectable }    from '@angular/core';
import {URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {User} from '../model/user'
import 'rxjs/add/operator/toPromise';
import {BaseService} from "./base.service";

@Injectable()
export class UsersService extends BaseService {


    login() : Observable<any> {
        this.params.set('access_token', Cookie.get('loginHash'));

        return this.http.get(this.apiUrl+'/users/login', {search: this.params})
            .map(response => response.json())
            .catch((error: any) => {return Observable.throw(error);});
    }

    set_subscription_email(user: User) {
        this.params.set('email_subscribe', user.email_subscribe);
        return this.http.post(this.apiUrl+ '/users/email-subscribe', this.params)
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }

    set_after_registration_popup(){
        return this.http.post(this.apiUrl+ '/users/after-registration-popup', this.params)
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }
}
