import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Work} from "../model/work";
import {BaseService} from "./base.service";
import {Card} from "../model/card";
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Injectable()
export class WorkService extends BaseService {

    getWorks() {
        return this.http.get(this.apiUrl + '/works', {search: this.params})
            .map(response => response.json())
            .catch((error) =>{ return Observable.throw(error);});
    }

    getWorksExample() {
        return this.http.get(this.apiUrl + '/works/exampleall', {search: this.params})
            .map(response => response.json())
            .catch((error) =>{ return Observable.throw(error);});
    }

    getWork(id) {
        return this.http.get(this.apiUrl + '/works/'+id, {search: this.params})
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }

    getWorkExample(id) {
        return this.http.get(this.apiUrl + '/works/example/'+id, {search: this.params})
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }


    update(id, work: Work, cards:Card[]) {
        this.params.set('workCards', JSON.stringify(cards));
        this.params.set('inputWorkTheses', JSON.stringify(work.theses));

        this.params.set('begin_count', String(work.begin_count));
        this.params.set('center_count', String(work.center_count));
        this.params.set('finish_count', String(work.finish_count));

        return this.http.put(this.apiUrl+ '/works/'+id, this.params)
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }


    create(work: Work) {
        this.params.set('name', work.name);
        this.params.set('inputWorkTheses', JSON.stringify(work.theses));
        this.params.set('time', work.time);

        let workCards = Cookie.get('workCards');
        Cookie.delete('workCards');

        this.params.set('workCards', workCards);


        return this.http.post(this.apiUrl+ '/works', this.params)
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }

    delete(work: Work) {
        return this.http.delete(this.apiUrl+ '/works/'+work.id, {search: this.params})
            .map(response => response.json())
            .catch((error:any) =>{ return Observable.throw(error);});
    }

}
