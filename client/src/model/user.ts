export class User {
    id: string;
    name: string;
    img: string;
    after_register_popup: string;
    email_subscribe: string;
}
