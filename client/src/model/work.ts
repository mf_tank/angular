export class Work {
    id: string;
    user_id: string;
    name: string;
    theses: Array<Theses>;
    time: string;
    status: number;

    begin_count:  number;
    center_count: number;
    finish_count: number;

    lang: string;
    created_at: string;
    cards: Array<string>;
}

export class Theses {
    title: string;
}