export class Card {
    id: string;
    name: string;
    time: string;
    cardUser;

    workCardTime: string;

    steps: Array<string> = [];
    cardText: Array<string> = [];
    stepsCount: Array<number> = [1, 2, 3, 4, 5];

    targetPupils: Array<string>;
    targetTeachers: Array<string>;
    is_big_groups: string = '0';

    message: string = null;

    color_type: string = '1';
    color_type_name: string;
    colorTypeArray: Array<string> = ['Понижение', 'Повышение'];

    individual_type: string = '1';
    individualTypeArray: Array<string> = ['Индивидуальная', 'Групповая'];

    //где можно использовать (начало середина или конец)
    use_type: Array<string> = [];
    useTypeArray: Array<string> = ['Начало', 'Суть', 'Финал'];

    //флаг, чтобы не закрывать инпут при редактировании в случае пустой строки
    is_editing: boolean = false;
}
