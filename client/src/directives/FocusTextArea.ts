
import { Directive, ElementRef, AfterViewInit,Renderer, } from '@angular/core';

@Directive({selector : '[initFocus]'})

export class FocusTextarea  implements AfterViewInit {

    constructor(public renderer: Renderer, public elementRef: ElementRef) {
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.elementRef.nativeElement.focus();
        },1);
    }
}