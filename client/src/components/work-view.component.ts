import {Component, OnInit, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

import {WorkService} from '../service/work.service';
import {Work} from "../model/work";
import {Card} from "../model/card";
import {PrintModalComponent} from "./print-modal.component";
import {CardsService} from "../service/cards.service";
declare var jQuery: any;

@Component({
    selector: 'work-view',
    providers: [WorkService, CardsService],
    templateUrl: '../views/work-view.html',
    encapsulation: ViewEncapsulation.None
})


export class WorkViewComponent implements OnInit, OnDestroy {

    workId: number;
    work : Work = new Work;
    works;
    filter;
    filterCount: string;

    cardDrops: Array<Card> = [];
    cards : Array<Card> = [];

    timeLine: Array<number> = [];
    error:any;
    errors : Array<string> = [];

    private subscription: Subscription;


    @ViewChild(PrintModalComponent) printModalComponent: PrintModalComponent
    constructor(
        private router: Router,
        private workService: WorkService,
        private activatedRoute: ActivatedRoute,
        private cardsService: CardsService
    ) {}


    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe(
            params => {
                this.workId = params['id'];
                this.workService.getWorkExample(this.workId)
                    .subscribe(
                        data => {
                            this.work = data;

                            if(data.cards) {
                                this.cardDrops = data.cards;
                                var i;
                                for(i=0;i<this.cardDrops.length;i++){
                                    this.cardDrops[i].message = data.workCards[i].text;
                                    this.cardDrops[i].workCardTime = data.workCards[i].time;
                                }
                                this.filterCount = data.cards.length;
                            }
                            this.timeChange();
                        },
                        error => {
                            this.workService.errors(error);
                        }
                    );
            },
        );


        this.cardsService.getFilterCards(this.workId, [])
            .subscribe(
                data => {
                    this.cards = data;
                    this.filterCount = data.length;
                },
                error => {
                    this.workService.errors(error);
                }
            );

        this.cardsService.getFilter()
            .subscribe(
                data => {
                    this.filter = data
                },
                error => {
                    this.cardsService.errors(error);
                }
            );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    openPrint(event) {
        this.printModalComponent.open();
    }

    openEdit(event) {
        this.router.navigate(['/work/update/', this.workId]);
    }

    timeChange() {
        let i;
        let time = 0;
        for(i in this.cardDrops) {
            time += Number(this.cardDrops[i].workCardTime);
        }

        this.timeLine = [];
        for(i=time+1; i<=this.work.time; i++) {
            this.timeLine.push(i);
        }
        jQuery('ul.timeline-scale').css('margin-left', (this.cardDrops.length*198)+'px');
    }

    close($event) {
        this.router.navigate(['work/']);
    }

}
