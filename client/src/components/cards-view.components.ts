import { Component, ViewChild, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { Work } from '../model/work';
import {CardsService} from "../service/cards.service";
import {Card} from "../model/card";
declare var jQuery: any;

@Component({
    selector: 'cards-view',
    templateUrl: '../views/cards-view.html',
    providers: [CardsService],
    encapsulation: ViewEncapsulation.None
})


export class CardsViewComponent {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    backdropOptions = [true, false, 'static'];
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    work: Work = new Work();
    errors : Array<string> = [];
    responseWork;
    done: boolean = false;

    error: any;

    card: Card = new Card;

    constructor(
        private router: Router,
        private cardsService: CardsService
    ) { }


    close(event) {
        this.modal.close();
    }

    open(id) {
        jQuery('.loader').removeClass('hidden');
        this.cardsService.getCard(id)
            .subscribe(
                data => {
                    this.card = data;

                    jQuery('.madal-newpadd .tabs-navigation li a').removeClass('active');
                    jQuery('.madal-newpadd .tabs-navigation li:first-child a').addClass('active');

                    jQuery('.madal-newpadd .tabs-container .itemTab').addClass('hidden');
                    jQuery('.madal-newpadd .tabs-container #tab-1').removeClass('hidden');

                    this.modal.open();

                    jQuery('.loader').addClass('hidden');
                },
                error => {
                    this.error = error;
                    this.cardsService.errors(error);
                }
            );
    }

    tabSwitch(event, number) {
        jQuery('.madal-newpadd .tabs-navigation li a').removeClass('active');
        jQuery(event.target).addClass('active');

        jQuery('.madal-newpadd .tabs-container .itemTab').addClass('hidden');
        jQuery('.madal-newpadd .tabs-container #tab-'+number).removeClass('hidden');
    }

    prevView(event, id) {
        let prev = jQuery('.content .itemCard#'+id+', .content .no-padd#'+id).prev();
        if(prev.length == 0) {
            prev = jQuery('.content .itemCard, .content .no-padd').last();
        }
        this.modal.close();
        this.open(prev.attr('id'))
    }

    nextView(event, id) {
        let next = jQuery('.content .itemCard#'+id+', .content .no-padd#'+id).next();
        if(next.length == 0) {
            next = jQuery('.content .itemCard, .content .no-padd').first();
        }
        this.modal.close();
        this.open(next.attr('id'))
    }

    @Output() enterViewModal = new EventEmitter<any>();
    enterView(card: Card) {
        this.modal.close();
        this.enterViewModal.emit(card);
    }

    like(event, card : Card) {
        if(jQuery(event.target).hasClass('active')) {
            jQuery(event.target).removeClass('active');
        } else {
            jQuery(event.target).addClass('active');
        }

        this.cardsService.setLike(card)
            .subscribe(
                data => {

                },
                error => {
                    this.cardsService.errors(error);
                }
            );
    }
}
