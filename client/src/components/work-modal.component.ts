import { Component, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { WorkService } from '../service/work.service';
import { Work } from '../model/work';
declare var jQuery: any;


@Component({
    selector: 'modal-work',
    templateUrl: '../views/work-modal.html',
    providers: [WorkService],
    encapsulation: ViewEncapsulation.None
})


export class WorkModalComponent implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    backdropOptions = [true, false, 'static'];
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    work: Work = new Work();

    errors : Array<string> = [];
    responseWork;
    done: boolean = false;

    error: any;

    constructor(
        private router: Router,
        private workService: WorkService
    ) { }

    ngOnInit() {
        this.work.theses = [{title:''}];
    }

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    onSubmit(work) {
        this.errors = [];

        this.workService.create(work)
            .subscribe(
                data => {
                    this.responseWork = data;
                    this.done = true;
                    this.modal.close();
                    this.router.navigate(['work/update/', this.responseWork.id]);
                },
                error => {
                    if(error.status == 422) {
                        this.done = true;
                        let data = jQuery.parseJSON(error._body);
                        for(let index in data[0]) {
                            if(index == 'message') {
                                this.errors.push(data[0][index]);
                            }
                        }
                    }
                    this.workService.errors(error);
                }
            );

    }

    addTheses () {
        this.work.theses.push({title:''});
        return false;
    }

    heightChange($event) {
        let content = $event.target.value;
        $event.target.nextElementSibling.innerHTML = "a"+content;
        $event.target.style.height = $event.target.nextElementSibling.scrollHeight + "px";
    }

    trackByFn (index,item){
        return index;
    }
}