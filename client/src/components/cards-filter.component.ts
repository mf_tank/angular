import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CardsService} from '../service/cards.service';
import {Subscription} from 'rxjs/Subscription';
import {Card} from "../model/card";
import {CardsViewComponent} from "./cards-view.components";
import {CardsCreateComponent} from "./cards-create.component";
import {Cookie} from 'ng2-cookies/ng2-cookies';
import {WorkModalComponent} from "./work-modal.component";
import {UsersService} from "../service/users.service";
import {LoginComponent} from "./login.component";
import {HelpModalComponent} from "./help-modal.component";
import {HelpModal2Component} from "./help-modal2.component";


declare var jQuery: any;

@Component({
    selector: 'cards-filter',
    providers: [CardsService, UsersService],
    templateUrl: '../views/cards-filter.html',
    encapsulation: ViewEncapsulation.None
})


export class CardsFilterComponent implements OnInit, OnDestroy {

    private subscription: Subscription;

    error: any;
    param: string;

    cards: Card[] = [];
    workCards: Array<string> = [];
    filter;

    @ViewChild(CardsCreateComponent) cardsCreateComponent: CardsCreateComponent
    @ViewChild(CardsViewComponent) cardsViewComponent: CardsViewComponent
    @ViewChild(WorkModalComponent) workModalComponent: WorkModalComponent
    @ViewChild(LoginComponent) loginComponent: LoginComponent
    @ViewChild(HelpModalComponent) helpModalComponent: HelpModalComponent
    @ViewChild(HelpModal2Component) helpModal2Component: HelpModal2Component
    constructor(
        private router: Router,
        private activateRoute: ActivatedRoute,
        private cardsService: CardsService,
        private usersService: UsersService,
    ) {}


    ngOnInit() {
        this.subscription = this.activateRoute.params.subscribe(
            params => {

                if(typeof params['param'] == 'undefined') {
                    this.param = 'energyUp'
                } else {
                    this.param = params['param'];
                }

                this.cardsService.getFilter()
                    .subscribe(
                        data => {
                            this.filter = data
                        },
                        error => {
                            this.error = error;
                            this.cardsService.errors(error);
                        }
                    );

                this.cardsService.getCards(this.param)
                    .subscribe(
                        data => {
                            this.cards = data
                        },
                        error => {
                            this.error = error;
                            this.cardsService.errors(error);
                        }
                    );
            },
        );

        Cookie.set('isWorkCreate', '0', 0);
        if(!Cookie.get('isHelpModal')  && !Cookie.get('isMethodCreate')) {
            this.helpModalComponent.open();
            Cookie.set('isHelpModal', '1', 0);
        }

        if(Cookie.get('isMethodCreate') == '1') {
            Cookie.set('isMethodCreate', '0', 0);
            this.openMethodCreate();
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


    openMethodCreate() {
        if(!Cookie.get('isHelpModal2')) {
            this.helpModal2Component.open();
            Cookie.set('isHelpModal2', '1', 0);
        } else {
            this.cardsCreateComponent.open();
        }
    }


    openView(event, id) {
        this.cardsViewComponent.open(id);
    }


    createCard(event) {
        console.log(2);
        this.usersService.login()
            .subscribe(
                data => {
                    this.openMethodCreate();
                },
                error => {
                    Cookie.set('isMethodCreate', '1', 0);
                    this.loginComponent.open();
                    this.usersService.errors(error);
                }
            );
    }

    like(event, card : Card) {
        if(jQuery(event.target).hasClass('active')) {
            jQuery(event.target).removeClass('active');
        } else {
            jQuery(event.target).addClass('active');
        }

        this.cardsService.setLike(card)
            .subscribe(
                data => {

                },
                error => {
                    this.cardsService.errors(error);
                }
            );
    }

    addWork(card : Card) {
        jQuery('.loader').removeClass('hidden');
        if(this.workCards.indexOf(card.id) == -1) {
            this.workCards.push(card.id);
        }
        Cookie.set('workCards', this.workCards.join(','), 0);

        this.usersService.login()
            .subscribe(
                data => {
                    jQuery('.loader').addClass('hidden');
                    this.workModalComponent.open();
                },
                error => {
                    jQuery('.loader').addClass('hidden');
                    this.loginComponent.open();
                    this.usersService.errors(error);
                }
            );
    }

    onClose() {
        this.cardsCreateComponent.open();
    }

    enterViewModal(card: Card) {
        this.addWork(card);
    }

}
