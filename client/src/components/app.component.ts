import {Component, ViewChild, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';
import {Cookie} from 'ng2-cookies/ng2-cookies';
import {UsersService} from "../service/users.service";
import {OrderService} from "../service/order.service";
import {LoginComponent} from "./login.component";
import {AfterRegisterComponent} from './modal/after-register.component';
import {PaidModalComponent} from './modal/paid.component';
import {User} from "../model/user";
import {PaidWinModalComponent} from "./modal/paidwin.component";
declare var jQuery: any;

@Component({
    selector: 'app',
    templateUrl: '../views/app.html',
    providers: [UsersService, OrderService],
})

export class AppComponent implements OnInit {

    isAuth: boolean = false;
    error: any;
    user: User = new User;

    @ViewChild(LoginComponent) loginComponent: LoginComponent;
    @ViewChild(AfterRegisterComponent) afterRegisterComponent: AfterRegisterComponent;
    @ViewChild(PaidWinModalComponent) paidWinModalComponent: PaidWinModalComponent;
    @ViewChild(PaidModalComponent) paidModalComponent: PaidModalComponent;
    constructor(
        private router: Router,
        private usersService: UsersService
    ) {}

    exit($event) {
        Cookie.delete('loginHash', '/');
        Cookie.delete('net', '/');
        this.isAuth = false;
        this.router.navigate(['']);
    }


    isTouchDevice() {
        try {
            document.createEvent('TouchEvent');
            return true;
        }
        catch(e) {
            return false;
        }
    }

    ngOnInit() {
        if(this.isTouchDevice()) {
            jQuery('.main').remove();
            jQuery('.az-recommend').removeClass('hidden')
        }

        this.router.events.subscribe((event) => {
            if(window.location.pathname == '/'){
                jQuery('.block_left').addClass('hidden');
                jQuery('.block_right').addClass('full_w');
            } else {
                jQuery('.block_left').removeClass('hidden');
                jQuery('.block_right').removeClass('full_w');
            }
            if(Cookie.get('showAfterRegisterPopup') == '1' && !jQuery('.modal').hasClass('in')){
                this.afterRegisterComponent.open();
            }
        });

        this.usersService.login()
            .subscribe(
                data => {
                    this.isAuth = true;
                    this.user = data;

                    if(data.after_register_popup == 1){
                        if(!jQuery('.modal').hasClass('in')) {
                            this.afterRegisterComponent.open();
                        } else {
                            Cookie.set('showAfterRegisterPopup', '1', 0, '/');
                        }
                    } else if(window.location.pathname == '/'){
                            this.router.navigate(['/cards/filters/energyUp']);
                    }
                },
                error => {
                    this.isAuth = false;
                    this.error = error;
                    this.usersService.errors(error);
                }
            );
        if(window.location.pathname == '/paidwin'){
            this.paidWinModalComponent.open();
        }
    }

    openLogin() {
        this.loginComponent.open();
    }

    openPaid() {
        this.paidModalComponent.open();
    }
}
