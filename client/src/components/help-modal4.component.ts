import { Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

declare var jQuery: any;

@Component({
    selector: 'help-modal4',
    templateUrl: '../views/help-modal4.html',
})

export class HelpModal4Component implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    errors : Array<string> = [];
    error: any;
    done: boolean = false;


    constructor(
        private router: Router,
    ) {}

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    ngOnInit() {

    }

    slideOpen(value) {
        jQuery('.helpModal4 ul li span').removeClass('ah-modal2-spanactive');
        jQuery('.helpModal4 ul li span[data-slide="'+value+'"]').addClass('ah-modal2-spanactive');

        jQuery('.helpModal4 .ah-modal2-wrapper').addClass('hidden');
        jQuery('.helpModal4 .ah-modal2-wrapper[data-slide="'+value+'"]').removeClass('hidden')
    }


    prevSlide(event) {
        let slide = jQuery('.helpModal4 span.ah-modal2-spanactive').attr('data-slide');
        let prev = Number(slide) - 1;
        if(prev >= 1) {
            this.slideOpen(prev);
        }
    }

    nextSlide(event) {
        let slide = jQuery('.helpModal4 span.ah-modal2-spanactive').attr('data-slide');
        let next = Number(slide) + 1;
        if(next <= 7) {
            this.slideOpen(next);
        }
        if(next == 8) {
            this.close();
        }
    }

}
