import { Component, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { WorkService } from '../service/work.service';
import { Work } from '../model/work';
import {Card} from "../model/card";
declare var jQuery: any;

@Component({
    selector: 'work-update',
    templateUrl: '../views/work-update.html',
    providers: [WorkService],
    encapsulation: ViewEncapsulation.None
})


export class WorkUpdateComponent{

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    backdropOptions = [true, false, 'static'];
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    @Input() cardDrops: Array<Card> = [];
    @Input() workId: number;
    @Input() work: Work = new Work();
    timeLine: Array<number> = [];
    errors : Array<string> = [];
    done: boolean = false;
    error: any;

    constructor(
        private router: Router,
        private workService: WorkService
    ) { }

    open() {
        this.modal.open();
    }


    onSubmit(work) {
        let data = this.workService.create(work);
        this.errors = [];

        this.workService.update(this.workId,this.work,this.cardDrops)
            .subscribe(
                data => {
                    this.work = data;
                    let i;

                    this.timeLine = [];
                    for(i=1; i<=this.work.time; i++) {
                        this.timeLine.push(i);
                    }
                    this.done = true;
                    if(data.theses.length == 0){
                        this.work.theses = [{title:''}];
                    }
                    this.modal.close();
                    this.router.navigate(['work/update', this.workId]);
                },
                error => {
                    if(error.status == 422) {
                        this.done = true;
                        let data = jQuery.parseJSON(error._body);
                        for(let index in data[0]) {
                            if(index == 'message') {
                                this.errors.push(data[0][index]);
                            }
                        }
                    }
                    this.workService.errors(error);
                }
            );
    }

    addTheses () {
        this.work.theses.push({title:''});
        return false;
    }

    height(i) {
        if(document.getElementById("dv_"+i)) {
            if(document.getElementById("dv_"+i).scrollHeight) {
                let content = jQuery('#txt_'+i).val();
                jQuery('#dv_'+i).html("a"+content);
                return document.getElementById("dv_" + i).scrollHeight;
            }
        }
    }

    trackByFn (index,item){
        return index;
    }
}


