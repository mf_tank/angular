import { Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

declare var jQuery: any;

@Component({
    selector: 'help-modal3',
    templateUrl: '../views/help-modal3.html',
})

export class HelpModal3Component implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    errors : Array<string> = [];
    error: any;
    done: boolean = false;


    constructor(
        private router: Router,
    ) {}

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    ngOnInit() {

    }

    @Output() onClose = new EventEmitter<boolean>();
    closeParent() {
        //if(!jQuery(event.target).is('span.ah-modal2-span') && !jQuery(event.target).is('a')) {
            this.modal.close();
            this.onClose.emit();
       // }
    }

    slideOpen(value) {
        jQuery('.helpModal3 ul li span').removeClass('ah-modal2-spanactive');
        jQuery('.helpModal3 ul li span[data-slide="'+value+'"]').addClass('ah-modal2-spanactive');

        jQuery('.helpModal3 .ah-modal2-wrapper').addClass('hidden');
        jQuery('.helpModal3 .ah-modal2-wrapper[data-slide="'+value+'"]').removeClass('hidden')
    }


    prevSlide(event) {
        let slide = jQuery('.helpModal3 span.ah-modal2-spanactive').attr('data-slide');
        let prev = Number(slide) - 1;
        if(prev >= 1) {
            this.slideOpen(prev);
        }
    }

    nextSlide(event) {
        let slide = jQuery('.helpModal3 span.ah-modal2-spanactive').attr('data-slide');
        let next = Number(slide) + 1;
        if(next <= 2) {
            this.slideOpen(next);
        }
        if(next == 3) {
            this.closeParent();
        }
    }

}
