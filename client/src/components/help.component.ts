import {Component, OnInit} from '@angular/core';
import {WorkService} from '../service/work.service';
import {Work} from "../model/work";
import {Cookie} from 'ng2-cookies/ng2-cookies';
declare var jQuery: any;


@Component({
    selector: 'help',
    templateUrl: '../views/help.html',
    providers: [WorkService]
})


export class HelpComponent implements OnInit {


    works: Array<Work> = [];


    constructor(private workService: WorkService) {
    }


    ngOnInit() {
        this.workService.getWorksExample()
            .subscribe(
                data => {this.works = data},
                error => {
                    this.workService.errors(error);
                }
            );


        Cookie.set('isMethodCreate', '0', 0);
        Cookie.set('isWorkCreate', '0', 0);
    }


    switchWork(event, value) {
        jQuery('.ah-help-list li').removeClass('active');
        jQuery(event.target).addClass('active');

        jQuery('.content .works .item').addClass('hidden');
        jQuery('.content .works .item[data-status="'+value+'"]').removeClass('hidden');
    }


    pageOpen(event) {
        var link = jQuery(event.target).closest('.item').children('.text');
        if(link.hasClass('hidden')) {
            link.removeClass('hidden');
        } else {
            link.addClass('hidden');
        }
    }

}
