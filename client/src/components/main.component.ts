import {Component, OnInit, ViewChild} from '@angular/core';
import {WorkModalComponent} from "./work-modal.component";
import {LoginComponent} from "./login.component";
import {UsersService} from "../service/users.service";
import {HelpModal3Component} from "./help-modal3.component";

import {Cookie} from 'ng2-cookies/ng2-cookies';

declare var jQuery: any;
declare var yaCounter43968854: any;
declare var ga: any;
@Component({
    selector: 'main',
    templateUrl: '../views/main.html',
    providers: [UsersService],
    styleUrls: [
        '../css/tilda-blocks-2.11.css',
        '../css/tilda-grid-3.0.min.css',
        '../css/custom.css'
    ]
})

export class MainComponent implements OnInit {

    error: any;
    auth: boolean = false;

    @ViewChild(LoginComponent) loginComponent: LoginComponent
    @ViewChild(WorkModalComponent) workModalComponent: WorkModalComponent
    @ViewChild(HelpModal3Component) helpModal3Component: HelpModal3Component

    constructor(
        private usersService: UsersService
    ) {}

    ngOnInit() {
        this.usersService.login()
            .subscribe(
                data => {
                    this.auth = true;
                },
                error => {
                    this.auth = false;
                    jQuery('.ah-workswrapp-span').removeClass('active');
                    jQuery('.ah-workswrapp-span.primer').addClass('active');

                    this.usersService.errors(error);
                }
            );
    }

    workCreate(event) {
        this.usersService.login()
            .subscribe(
                data => {
                    if(!Cookie.get('isHelpModal3')) {
                        this.helpModal3Component.open();
                        Cookie.set('isHelpModal3', '1', 0);
                    } else {
                        this.workModalComponent.open();
                    }
                },
                error => {
                    this.loginComponent.open();
                    this.usersService.errors(error);
                }
            );
    }

    onClose() {
        this.workModalComponent.open();
    }
    openLogin() {
        this.loginComponent.open();
    }
}
