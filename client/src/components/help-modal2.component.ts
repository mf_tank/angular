import { Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

declare var jQuery: any;

@Component({
    selector: 'help-modal2',
    templateUrl: '../views/help-modal2.html',
})

export class HelpModal2Component implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    errors : Array<string> = [];
    error: any;
    done: boolean = false;


    constructor(
        private router: Router,
    ) {}

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    @Output() onClose = new EventEmitter<boolean>();
    closeParent() {
        this.modal.close();
        this.onClose.emit();
    }


    ngOnInit() {

    }

}
