import {Component, OnInit, ViewChild} from '@angular/core';
import { Router} from '@angular/router';
import { WorkService } from '../service/work.service';
import {WorkModalComponent} from "./work-modal.component";
import {LoginComponent} from "./login.component";
import {UsersService} from "../service/users.service";
import {Work} from "../model/work";
import {HelpModal3Component} from "./help-modal3.component";
import {Cookie} from 'ng2-cookies/ng2-cookies';
declare var jQuery: any;

@Component({
    selector: 'works',
    providers: [WorkService, UsersService],
    templateUrl: '../views/work.html'
})

export class WorkComponent implements OnInit {

    works: Array<Work> = [];
    worksExample: Array<Work> = [];
    error: any;

    auth: boolean = false;

    @ViewChild(LoginComponent) loginComponent: LoginComponent
    constructor(
        private router: Router,
        private workService: WorkService,
        private usersService: UsersService
    ) {}


    @ViewChild(WorkModalComponent) workModalComponent: WorkModalComponent
    @ViewChild(HelpModal3Component) helpModal3Component: HelpModal3Component
    ngOnInit() {
        this.usersService.login()
            .subscribe(
                data => {
                    this.auth = true;
                },
                error => {
                    this.auth = false;
                    jQuery('.ah-workswrapp-span').removeClass('active');
                    jQuery('.ah-workswrapp-span.primer').addClass('active');

                    this.usersService.errors(error);
                }
            );


        this.workService.getWorks()
            .subscribe(
                data => {
                    this.works = data;
                    if(data.length == 0 && !Cookie.get('isWorkCreate')) {
                        this.openWorkCreate();
                    }
                },
                error => {
                    this.error = error;
                    this.workService.errors(error);
                }
            );

        this.workService.getWorksExample()
            .subscribe(
                data => {
                    this.worksExample = data;
                },
                error => {
                    this.error = error;
                    this.workService.errors(error);
                }
            );

        Cookie.set('isMethodCreate', '0', 0);
        if(Cookie.get('isWorkCreate') == '1') {
            Cookie.set('isWorkCreate', '0', 0);
            this.openWorkCreate();
        }
    }

    openWorkCreate() {
        if(!Cookie.get('isHelpModal3')) {
            this.helpModal3Component.open();
            Cookie.set('isHelpModal3', '1', 0);
        } else {
            this.workModalComponent.open();
        }
    }

    workCreate(event) {
        this.usersService.login()
            .subscribe(
                data => {
                    this.openWorkCreate();
                },
                error => {
                    Cookie.set('isWorkCreate', '1', 0);
                    this.loginComponent.open();
                    this.usersService.errors(error);
                }
            );
    }


    switchWork(event, value) {
        if(!this.auth && value == 1) {
            this.loginComponent.open();
        } else {
            jQuery('.ah-workswrapp-span').removeClass('active');
            jQuery(event.target).addClass('active');

            jQuery('.content .works .item').addClass('hidden');
            jQuery('.content .works .item[data-status="'+value+'"]').removeClass('hidden');
        }
    }

    onClose() {
        this.workModalComponent.open();
    }

    deleteWork(event, work) {
        jQuery('.loader').removeClass('hidden');
        this.workService.delete(work)
            .subscribe(
                data => {
                    jQuery(event.target).closest('.item').remove();
                    jQuery('.loader').addClass('hidden');
                },
                error => {
                    jQuery('.loader').addClass('hidden');
                    this.error = error;
                    this.workService.errors(error);
                }
            );
    }
}
