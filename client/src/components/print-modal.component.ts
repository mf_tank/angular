import {Component, OnInit, ViewChild, Input, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';
import {Work} from "../model/work";
import {isUndefined} from "util";
import { environment } from '../environments/environment';

declare var jQuery: any;

@Component({
    selector: 'print-modal',
    templateUrl: '../views/print-modal.html',
})

export class PrintModalComponent implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    backdropOptions = [true, false, 'static'];
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    errors : Array<string> = [];
    error: any;
    done: boolean = false;

    @Input() work : Work = new Work;

    timeStartHours : number;
    timeStartMinute : number;
    timeDownHours : number;
    timeDownMinute : number;

    constructor(
        private router: Router,
    ) {}

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    ngOnInit() {

    }

    timeStartUp(event, name) {
        if(isUndefined(this.timeStartHours)) {
            this.timeStartHours = 0;
        }
        if(isUndefined(this.timeStartMinute)) {
            this.timeStartMinute = 0;
        }

        if(name == 'hours') {
            if(this.timeStartHours < 24) {
                this.timeStartHours += 1;
            }
        } else if(name == 'minute') {
            if(this.timeStartMinute < 60) {
                this.timeStartMinute += 1;
            }
        }
    }

    timeStartDown(event, name) {
        if(isUndefined(this.timeStartHours)) {
            this.timeStartHours = 0;
        }
        if(isUndefined(this.timeStartMinute)) {
            this.timeStartMinute = 0;
        }


        if(name == 'hours') {
            if(this.timeStartHours > 0) {
                this.timeStartHours -= 1;
            }
        } else if(name == 'minute') {
            if(this.timeStartMinute > 0) {
                this.timeStartMinute -= 1;
            }
        }
    }

    timeEndUp(event, name) {
        if(isUndefined(this.timeDownHours)) {
            this.timeDownHours = 0;
        }
        if(isUndefined(this.timeDownMinute)) {
            this.timeDownMinute = 0;
        }

        if(name == 'hours') {
            if(this.timeDownHours < 24) {
                this.timeDownHours += 1;
            }
        } else if(name == 'minute') {
            if(this.timeDownMinute < 60) {
                this.timeDownMinute += 1;
            }
        }
    }

    timeEndDown(event, name) {
        if(isUndefined(this.timeDownHours)) {
            this.timeDownHours = 0;
        }
        if(isUndefined(this.timeDownMinute)) {
            this.timeDownMinute = 0;
        }

        if(name == 'hours') {
            if(this.timeDownHours > 0) {
                this.timeDownHours -= 1;
            }
        } else if(name == 'minute') {
            if(this.timeDownMinute > 0) {
                this.timeDownMinute -= 1;
            }
        }
    }

    print($event) {
        jQuery(event.target).closest('a').attr(
            'href',
            environment.url+'/pdf/'+this.work.id+'/'+this.timeStartHours+':'+this.timeStartMinute
        );
    }
}
