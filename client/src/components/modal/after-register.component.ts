import { Component, ViewChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { UsersService } from '../../service/users.service';
import {User} from '../../model/user';
import { Router } from '@angular/router';
import {Cookie} from 'ng2-cookies/ng2-cookies';
declare var jQuery: any;

@Component({
    selector: 'modal-after-register',
    templateUrl: '../../views/modal/after-register.html',
})

export class AfterRegisterComponent {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;
    errors : Array<string> = [];
    responseUser;
    done: boolean = false;
    user: User = new User ();
    error: any;

    constructor(
        private router: Router,
        private usersService: UsersService
    ) { }

    open() {
        this.modal.open();
    }

    close() {
        this.usersService.set_after_registration_popup()
            .subscribe(
                data => {
                    this.user.after_register_popup = '0';
                },
                error => {
                }
            );
        Cookie.set('showAfterRegisterPopup', '0', 0, '/');
        this.modal.close();
    }

    slideOpen(value) {
        jQuery('.afterRegister .ah-modal2-wrapper').addClass('hidden');
        jQuery('.afterRegister .ah-modal2-wrapper[data-slide="'+value+'"]').removeClass('hidden')
    }

    onSubmit(user) {
        this.errors = [];

        this.usersService.set_subscription_email(user)
            .subscribe(
                data => {
                    this.responseUser = data;
                    this.done = true;
                    this.user.after_register_popup = '0';
                    this.slideOpen(2);
                },
                error => {
                    if(error.status == 422) {
                        this.done = true;
                        let data = jQuery.parseJSON(error._body);
                        for(let index in data[0]) {
                            if(index == 'message') {
                                this.errors.push(data[0][index]);
                            }
                        }
                    }
                    this.usersService.errors(error);
                }
            );
    }
}
