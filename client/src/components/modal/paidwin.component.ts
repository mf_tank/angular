import { Component, ViewChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
declare var jQuery: any;


@Component({
    selector: 'modal-paidwin',
    templateUrl: '../../views/modal/paidwin.html',
})


export class PaidWinModalComponent {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    constructor() {}

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

}
