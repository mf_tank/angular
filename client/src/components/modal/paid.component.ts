import { Component, ViewChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import {OrderService} from '../../service/order.service';
declare var jQuery: any;


@Component({
    selector: 'modal-paid',
    templateUrl: '../../views/modal/paid.html',
})


export class PaidModalComponent {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    constructor(private orderService: OrderService) {}

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    orderCreate(event) {
        event.target.setAttribute('disabled', true);
        event.target.innerHTML = 'Подождите...';
        this.orderService.create()
            .subscribe(
                data => {
                    window.location.href = data.action + '?shopId=' + data.params.shopId + '&scid=' + data.params.scid
                        + '&customerNumber=' + data.params.customerNumber  + '&orderNumber=' + data.params.orderNumber + '&sum=' + data.params.sum;
                },
                error => {
                    this.orderService.errors(error);
                }
            );
    }
}
