import {Component, OnInit, ViewChild, ViewEncapsulation, Input} from '@angular/core';
import {Router} from '@angular/router';
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';
import {Cookie} from 'ng2-cookies/ng2-cookies';
import {UsersService} from "../service/users.service";
import {environment} from '../environments/environment';

declare var jQuery: any;
declare var yaCounter43968854: any;
declare var ga: any;

@Component({
    selector: 'login',
    templateUrl: '../views/login.html',
    encapsulation: ViewEncapsulation.None,
    providers: [UsersService],
})

export class LoginComponent implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    backdropOptions = [true, false, 'static'];
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    errors : Array<string> = [];
    error: any;
    done: boolean = true;
    terms: boolean = true;
    constructor(
        private router: Router,
        private usersService: UsersService
    ) {}

    open() {
        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    loginFb(event) {
        yaCounter43968854.reachGoal('social_reg_fb');
        ga('send', 'social_reg_fb');
        Cookie.set('net', 'facebook', 0, '/');
        var link = '';
        if(!environment.production) {

            link = 'https://www.facebook.com/v2.8/dialog/oauth?client_id=162360127604727&redirect_uri='+document.location.protocol+'//'+document.location.hostname+'/cards/filters/energyUp&display=popup&response_type=token&scope=email';
            /*
            jQuery(event.target).closest('a').attr(
                'href',
                'https://www.facebook.com/v2.8/dialog/oauth?client_id=162360127604727&redirect_uri='+document.location.protocol+'//'+document.location.hostname+'/cards/filters/energyUp&display=popup&response_type=token&scope=email',
            );
            */
        } else {
            /*
            jQuery(event.target).closest('a').attr(
                'href',
                'https://www.facebook.com/v2.8/dialog/oauth?client_id=801278483362957&redirect_uri='+document.location.protocol+'//'+document.location.hostname+'/cards/filters/energyUp&display=popup&response_type=token&scope=email',
            );
            */

            link = 'https://www.facebook.com/v2.8/dialog/oauth?client_id=801278483362957&redirect_uri='+document.location.protocol+'//'+document.location.hostname+'/cards/filters/energyUp&display=popup&response_type=token&scope=email';
        }
        window.location.href = link;
    }

    loginVk(event) {
        yaCounter43968854.reachGoal('social_reg_vk');
        ga('send', 'social_reg_vk');
        Cookie.set('net', 'vkontakte', 0, '/');
        var link = '';
        if(!environment.production) {
            link = 'https://oauth.vk.com/authorize?client_id=6034183&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token&v=5.52&scope=email';
            /*jQuery(event.target).closest('a').attr(
                'href',
                'https://oauth.vk.com/authorize?client_id=6034183&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token&v=5.52&scope=email'
            );
            */
        } else {
            link = 'https://oauth.vk.com/authorize?client_id=5954917&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token&v=5.52&scope=email';
            /*jQuery(event.target).closest('a').attr(
                'href',
                'https://oauth.vk.com/authorize?client_id=5954917&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token&v=5.52&scope=email'
            );
            */
        }
        window.location.href = link;
    }

    loginOd(event) {
        yaCounter43968854.reachGoal('social_reg_ok');
        ga('send', 'social_reg_ok');
        Cookie.set('net', 'odnoklassniki', 0, '/');
        var link = '';
        if(!environment.production) {
            link = 'https://connect.ok.ru/oauth/authorize?client_id=1249963520&scope=GET_EMAIL&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token';

            /*
            jQuery(event.target).closest('a').attr(
                'href',
                'https://connect.ok.ru/oauth/authorize?client_id=1249963520&scope=GET_EMAIL&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token'
            );
            */
        } else {
            link = 'https://connect.ok.ru/oauth/authorize?client_id=1250491136&scope=GET_EMAIL&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token';
            /*
            jQuery(event.target).closest('a').attr(
                'href',
                'https://connect.ok.ru/oauth/authorize?client_id=1250491136&scope=GET_EMAIL&redirect_uri='+document.location.protocol+'//'+document.location.host+'/cards/filters/energyUp&response_type=token'
            );
            */
        }
        window.location.href = link;
    }

    termsChange(event) {
        this.terms = !this.terms;
    }

    ngOnInit() {

    }

}
