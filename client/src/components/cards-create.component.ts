import { Component,  OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import {Card} from "../model/card";
import {CardsService} from "../service/cards.service";
import {TargetService} from "../service/target.service";
import {Target} from "../model/target";
declare var jQuery: any;

@Component({
    selector: 'card-create',
    templateUrl: '../views/card-create.html',
    providers: [CardsService, TargetService],
    encapsulation: ViewEncapsulation.None
})


export class CardsCreateComponent  implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    backdropOptions = [true, false, 'static'];
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    card: Card = new Card();

    targetPupils: Array<string> = [];
    targetTeachers: Array<string> = [];
    useTypes:  Array<string> = [];
    colorType: string;
    individualType: string;

    stepCount: number = 1;

    targetPupilsAll: Array<Target>;
    targetTeachersAll: Array<Target>;

    errors : Array<string> = [];
    done: boolean = false;


    constructor(
        private cardsService: CardsService,
        private targetService: TargetService,
        private router: Router
    ) {}

    open() {
        jQuery('.cardCreate .success').addClass('hidden');
        jQuery('.cardCreate form').removeClass('hidden');

        this.modal.open();
    }

    close() {
        this.modal.close();
    }

    onSubmit(card) {
        card.targetTeachers = this.targetTeachers;
        card.targetPupils = this.targetPupils;
        if(this.colorType) {
            card.color_type = this.colorType;
        }
        if(card.is_big_groups) {
            card.is_big_groups = '1'
        }
        card.use_type = this.useTypes;

        if(this.individualType) {
            card.individual_type = this.individualType;
        }

        this.errors = [];

        this.cardsService.create(card)
            .subscribe(
                data => {
                    this.done = true;
                    this.card = new Card();
                    jQuery('.cardCreate .success').removeClass('hidden');
                    jQuery('.cardCreate form').addClass('hidden');
                },
                error => {
                    if(error.status == 422) {
                        this.done = true;
                        let data = jQuery.parseJSON(error._body);
                        for(let index in data[0]) {
                            if(index == 'message') {
                                this.errors.push(data[0][index]);
                            }
                        }
                    }
                    this.cardsService.errors(error);
                }
            );
    }

    ngOnInit() {
        this.targetService.getTargetTeachers()
            .subscribe(
                data => this.targetTeachersAll = data,
                error => {
                    this.cardsService.errors(error);
                }
            );
        this.targetService.getTargetPupils()
            .subscribe(
                data => this.targetPupilsAll = data,
                error => {
                    this.cardsService.errors(error);
                }
            );
    }


    addStep(event:any) {
        this.stepCount++;
        let step = jQuery('.cardCreate .cardText .item:nth-child('+this.stepCount+')');
        step.removeClass('hidden');
        if(this.stepCount == 5) {
            jQuery(event.target).remove();
        }
    }


    selectTeacher(id, event:any) {
        if(jQuery(event.target).hasClass('active')) {
            for(let item in this.targetTeachers) {
                if(this.targetTeachers[item] == id) {
                    delete(this.targetTeachers[item]);
                    break;
                }
            }

            jQuery(event.target).removeClass('active');
        } else {
            jQuery(event.target).addClass('active');
            this.targetTeachers.push(id);
        }
    }

    selectPupil(id, event:any) {
        if(jQuery(event.target).hasClass('active')) {
            for(let item in this.targetPupils) {
                if(this.targetPupils[item] == id) {
                    delete(this.targetPupils[item]);
                    break;
                }
            }

            jQuery(event.target).removeClass('active');
        } else {
            jQuery(event.target).addClass('active');
            this.targetPupils.push(id);
        }
    }

    selectType(id, event:any) {
        if(jQuery(event.target).hasClass('active')) {
            for(let item in this.useTypes) {
                if(this.useTypes[item] == id) {
                    delete(this.useTypes[item]);
                    break;
                }
            }

            jQuery(event.target).removeClass('active');
        } else {
            jQuery(event.target).addClass('active');
            this.useTypes.push(id);
        }
    }

    selectColor(id, event:any) {
        jQuery('.form-group.colorType .item').removeClass('active');
        jQuery(event.target).addClass('active');
        this.colorType = id;
    }

    selectIndividual(id, event:any) {
        jQuery('.form-group.individualTypeArray .item').removeClass('active');
        jQuery(event.target).addClass('active');
        this.individualType = id;
    }


    changeBig(event, value) {
        this.card.is_big_groups = value;
    }
}




