import {Component, OnInit, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import { DragulaService } from 'ng2-dragula';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import {WorkService} from '../service/work.service';
import {CardsViewComponent} from "./cards-view.components";
import {Work} from "../model/work";
import {Card} from "../model/card";
import {PrintModalComponent} from "./print-modal.component";
import {CardsService} from "../service/cards.service";
import {CardsCreateComponent} from "./cards-create.component";
import {WorkUpdateComponent} from "./work-update.component";
import {HelpModal4Component} from "./help-modal4.component";
declare var jQuery: any;
declare var yaCounter43968854: any;
declare var ga: any;

@Component({
    selector: 'create-work',
    providers: [WorkService, CardsService],
    templateUrl: '../views/work-create.html'
})


export class WorkCreateComponent implements OnInit, OnDestroy {

    workId: number;
    work : Work = new Work;
    works;
    filter;
    filterCount: string;
    done: boolean = false;

    cardDrops: Array<Card> = [];
    cards : Array<Card> = [];
    private subscription: Subscription;

    timeLine: Array<number> = [];
    error:any;
    errors : Array<string> = [];

    unlock: boolean;
    statusBegin: any;
    statusHeart: any;
    statusFinal: any;
    statusBox: any;
    move_number: number;

    @ViewChild(CardsViewComponent) cardsViewComponent: CardsViewComponent
    @ViewChild(PrintModalComponent) printModalComponent: PrintModalComponent
    @ViewChild(CardsCreateComponent) cardsCreateComponent: CardsCreateComponent
    @ViewChild(WorkUpdateComponent) workUpdateComponent: WorkUpdateComponent
    @ViewChild(HelpModal4Component) helpModal4Component: HelpModal4Component
    constructor(
        private router: Router,
        private workService: WorkService,
        private activatedRoute: ActivatedRoute,
        private dragulaService: DragulaService,
        private cardsService: CardsService
    ) {

        const bag: any = this.dragulaService.find('first-bag');
        if (bag !== undefined ) this.dragulaService.destroy('first-bag');

        dragulaService.setOptions('first-bag', {
            revertOnSpill: true,
            copy: (el, source): boolean =>  {
                return source.className === 'cards';
            },
            removeOnSpill: (el: Element, source: Element): boolean => {
                return source.className !== 'cards';
            },
            accepts: (el: Element, target: Element, source: Element, sibling: Element): boolean => {
                return target.className !== 'cards';
            }
        });

        dragulaService.drop.subscribe((value) => {
            yaCounter43968854.reachGoal('metod_to_timeline');
            ga('send', 'metod_to_timeline');
            this.workService.update(this.workId, this.work, this.cardDrops)
                .subscribe(
                    data => {
                        if(jQuery('.timelane-box').hasClass('up')) {
                            jQuery('.timelane-box.up .messageBlock').removeClass('hidden');
                        }
                    },
                    error => {
                        this.workService.errors(error);
                    }
                );

            this.timeChange();
        });

        dragulaService.removeModel.subscribe((value) => {
            this.timeChange();
        });

        dragulaService.dropModel.subscribe((value) => {
            this.timeChange();
        });
    }

    createCard(event) {
        this.cardsCreateComponent.open();
    }

    timeChange() {
        let i;
        let time = 0;
        for(i in this.cardDrops) {
            time += Number(this.cardDrops[i].workCardTime);
        }

        this.timeLine = [];
        for(i=time+1; i<=this.work.time; i++) {
            this.timeLine.push(i);
        }
        jQuery('ul.timeline-scale').css('margin-left', (this.cardDrops.length*198)+'px');
    }

    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe(
            params => {
                this.workId = params['id'];
                this.workService.getWork(this.workId)
                    .subscribe(
                        data => {
                            this.work = data;

                            if(data.cards) {
                                this.cardDrops = data.cards;
                                var i;
                                for(i=0;i<this.cardDrops.length;i++){
                                    this.cardDrops[i].message = data.workCards[i].text;
                                    this.cardDrops[i].workCardTime = data.workCards[i].time;
                                }
                                this.filterCount = data.cards.length;
                            }
                            if(data.theses.length == 0){
                                this.work.theses = [{title:''}];
                            }
                            this.timeChange();
                        },
                        error => {
                            this.workService.errors(error);
                        }
                    );
            },
        );

        this.sendFilter();

        this.cardsService.getFilter()
            .subscribe(
                data => {
                    this.filter = data;
                },
                error => {
                    this.cardsService.errors(error);
                }
            );

        if(!Cookie.get('isHelpModal4')) {
            this.helpModal4Component.open();
            Cookie.set('isHelpModal4', '1', 0);
        }

        this.statusBegin = jQuery('.status-begin');
        this.statusHeart = jQuery('.status-heart');
        this.statusFinal = jQuery('.status-final');
        this.statusBox = jQuery('.status-box');
        jQuery(document).mousemove((e) => {

            if(this.unlock) {
                if(this.move_number == 1) {
                    this.statusBegin.css("width", e.pageX - 140 + jQuery('.bottom-timeline').scrollLeft());
                    this.statusHeart.css("width", this.statusBox.width() - this.statusFinal.width() - this.statusBegin.width() + 'px');
                } else {
                    this.statusHeart.css("width", e.pageX - 140 - this.statusBegin.width() + jQuery('.bottom-timeline').scrollLeft());
                    this.statusFinal.css("width", this.statusBox.width() - this.statusHeart.width() - this.statusBegin.width());
                }
            }
        });

        jQuery(document).mouseup((e) => {
            if(this.unlock) {
                this.unlock = false;
                jQuery('.resize').css("background-color", "rgba(0, 0, 0, 0.1)");
                this.work.begin_count = Math.round((this.statusBox.width() - this.statusHeart.width() - this.statusFinal.width()) * 100 / this.statusBox.width());
                this.work.finish_count = Math.round((this.statusBox.width() - this.statusBegin.width() - this.statusHeart.width()) * 100 / this.statusBox.width());
                this.work.center_count = 100 - this.work.begin_count - this.work.finish_count;
                this.workService.update(this.workId, this.work, this.cardDrops)
                    .subscribe(
                        data => {
                        },
                        error => {
                            this.workService.errors(error);
                        }
                    );
            }
        });
    }

    openView(event, id) {
        this.cardsViewComponent.open(id);
    }

    openPrint() {
        jQuery('.loader').removeClass('hidden');
        this.workService.update(this.workId,this.work,this.cardDrops)
            .subscribe(
                data => {
                    jQuery('.loader').addClass('hidden');
                    this.printModalComponent.open();
                },
                error => {
                    jQuery('.loader').addClass('hidden');
                    this.workService.errors(error);
                }
            );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    updateWork(event) {
        this.workUpdateComponent.open();
    }


    resizeStart(event, i){
        this.unlock = true;
        this.move_number = i;
        event.target.style.background = "rgba(0, 0, 0, 0.2)";
    }


    getContentPadding (){
        return 40 + (jQuery('.messageBlock').hasClass('hidden')?20:jQuery('.messageAdd').outerHeight()) + jQuery('.ah-container').outerHeight();
    }

    openFilter(event) {
        if(jQuery(event.target).hasClass('active')) {
            jQuery(event.target).removeClass('active');
            jQuery(event.target).html('Открыть фильтры');
            jQuery('.ah-container.filter').addClass('hidden');
        } else {
            jQuery(event.target).addClass('active');
            jQuery(event.target).html('Скрыть фильтры');
            jQuery('.ah-container.filter').removeClass('hidden');
        }
        jQuery('.main .block_right .content.work-create').css('padding-bottom', this.getContentPadding() + 'px');
        this.sendFilter();
    }


    openSort(event, elClass) {
        if(jQuery(event.target).hasClass('active')) {
            jQuery(event.target).removeClass('active');
            jQuery('.ah-container .filterClick ul').addClass('hidden');
            jQuery('ul.ah-workcreate-list3').addClass('hidden');
        } else {
            jQuery('.ah-container .ah-workcreate-list a').removeClass('active');
            jQuery(event.target).addClass('active');
            jQuery('.ah-container .filterClick ul').addClass('hidden');
            jQuery('.ah-container .filterClick ul.'+elClass).removeClass('hidden');
            jQuery('ul.ah-workcreate-list3').removeClass('hidden');
        }
        jQuery('.main .block_right .content.work-create').css('padding-bottom', this.getContentPadding() + 'px');
        this.sendFilter();
    }

    closeSort(event, elClass) {
        jQuery(event.target).prev().removeClass('cl');
        jQuery('.ah-container .filterClick ul.'+elClass+' span').removeClass('active');
        this.sendFilter();
    }

    closeLevel(event) {
        jQuery('.ah-workcreate-list2').addClass('hidden');
        jQuery('ul.ah-workcreate-list3').addClass('hidden');
        jQuery('.ah-container .ah-workcreate-list a').removeClass('active');
        jQuery('.main .block_right .content.work-create').css('padding-bottom', this.getContentPadding() + 'px');
    }

    onFilter(event) {
        if(jQuery(event.target).hasClass('active')) {
            jQuery(event.target).removeClass('active');
        } else {
            if(jQuery(event.target).closest('ul').is('.energy')) {
                jQuery('.ah-container .filterClick ul.energy span').removeClass('active');
            } else if(jQuery(event.target).closest('ul').is('.countPupils')) {
                jQuery('.ah-container .filterClick ul.countPupils span').removeClass('active');
            } else if(jQuery(event.target).closest('ul').is('.sort')) {
                jQuery('.ah-container .filterClick ul.sort span[data-name="'+jQuery(event.target).attr('data-name')+'"]').removeClass('active');
            }
            jQuery(event.target).addClass('active');
        }

        jQuery('.ah-workcreate-list a').removeClass('cl');
        jQuery('ul.ah-workcreate-list2 span.active').each(function () {
            let name = jQuery(this).closest('ul').attr('data-name');
            jQuery('.ah-workcreate-list a[data-name="'+name+'"]').addClass('cl');
        });

        this.sendFilter();
    }

    sendFilter() {
        let params = [];
        jQuery('ul.ah-workcreate-list2 span.active').each(function () {
            params.push({
                name: jQuery(this).attr('data-name'),
                value: jQuery(this).attr('data-value'),
            });
        });

        this.cardsService.getFilterCards(this.workId, params)
            .subscribe(
                data => {
                    this.cards = data;
                    this.filterCount = data.length;

                    let workCards = Cookie.get('workCards');
                    if(workCards) {
                        let addCards;
                        let i, j;
                        addCards = workCards.split(',');
                        for(i in addCards) {
                            for(j in data) {
                                if(data[j].id == addCards[i]) {
                                    this.addDrop(data[j]);
                                    break;
                                }
                            }
                        }
                    }

                },
                error => {
                    this.workService.errors(error);
                }
            );
    }

    addDrop(card) {
        this.cardDrops.push(card);
        this.workService.update(this.workId,this.work,this.cardDrops)
            .subscribe(
                data => {
                },
                error => {
                    this.workService.errors(error);
                }
            );
        this.timeChange();
    }

    openMessage(event) {
        if(jQuery('.messageBlock').hasClass('hidden')) {
            jQuery('.timelane-box').addClass('up');
            jQuery('.messageBlock').removeClass('hidden');
            jQuery(event.target).addClass('active');
            jQuery(event.target).html('Скрыть заметки');
        } else {
            jQuery('.timelane-box').removeClass('up');
            jQuery('.messageBlock').addClass('hidden');
            jQuery(event.target).removeClass('active');
            jQuery(event.target).html('Открыть заметки');
        }
        jQuery('.main .block_right .content.work-create').css('padding-bottom', this.getContentPadding() + 'px');
    }

    addMessage(event) {
        jQuery(event.target).addClass('hidden');
        jQuery(event.target).prev('.message').removeClass('hidden');
    }

    editMessage(event) {
        jQuery(event.target).closest('.message').children('.text').children('textarea').focus();
    }

    removeMessage(event, card: Card) {
        jQuery(event.target).closest('.message').addClass('hidden');
        card.message = null;
        this.inputMessage(event, card);
        jQuery(event.target).closest('.message').next('.messageAdd').removeClass('hidden');
    }

    focusOutMessage(event, card: Card) {
        console.log(card.message);
        if(card.message == null || card.message == '') {
            jQuery(event.target).closest('.message').addClass('hidden');
            jQuery(event.target).closest('.message').next('.messageAdd').removeClass('hidden');
        }
    }

    timerId: any;
    inputMessage(event, card: Card) {
        card.is_editing = true;
        const self = this;
        function updateWork() {
            self.workService.update(self.workId,self.work,self.cardDrops)
                .subscribe(
                    data => {
                        self.timerId = null;
                    },
                    error => {
                        self.workService.errors(error);
                    }
                );

        }
        if(this.timerId != null){
            clearTimeout(this.timerId);
        }
        this.timerId = setTimeout(updateWork, 3000);
    }

    minusTime($event, card: Card) {
        if(Number(card.workCardTime) > 5) {
            card.workCardTime = String(Number(card.workCardTime) - 1);

            this.workService.update(this.workId,this.work,this.cardDrops)
                .subscribe(
                    data => {
                        this.timeChange();
                    },
                    error => {
                        this.workService.errors(error);
                    }
                );
        }
    }

    addTime($event, card: Card) {
        card.workCardTime =  String(Number(card.workCardTime) + 1);

        this.workService.update(this.workId,this.work,this.cardDrops)
            .subscribe(
                data => {
                    this.timeChange();
                },
                error => {
                    this.workService.errors(error);
                }
            );
    }

    like(event, card : Card) {
        if(jQuery(event.target).hasClass('active')) {
            jQuery(event.target).removeClass('active');
        } else {
            jQuery(event.target).addClass('active');
        }

        this.cardsService.setLike(card)
            .subscribe(
                data => {

                },
                error => {
                    this.cardsService.errors(error);
                }
            );
    }


    save(event) {
        jQuery('.loader').removeClass('hidden');
        this.workService.update(this.workId,this.work,this.cardDrops)
            .subscribe(
                data => {
                    jQuery('.loader').addClass('hidden');
                },
                error => {
                    jQuery('.loader').addClass('hidden');
                    this.workService.errors(error);
                }
            );
    }


    enterViewModal(card: Card) {
        this.addDrop(card);
    }

}