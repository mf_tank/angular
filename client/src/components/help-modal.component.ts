import { Component, OnInit, ViewChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import {Cookie} from 'ng2-cookies/ng2-cookies';
import {AfterRegisterComponent} from './modal/after-register.component';
import { Router } from '@angular/router';
declare var jQuery: any;


@Component({
    selector: 'help-modal',
    templateUrl: '../views/help-modal.html',
    providers: [AfterRegisterComponent],
})


export class HelpModalComponent implements OnInit {

    @ViewChild('modal')
    modal: ModalComponent;
    @ViewChild('afterRegister')
    modal2: ModalComponent;
    output: string;
    selected: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = true;
    backdrop: string | boolean = true;
    css: boolean = false;

    constructor(private router: Router) {}

    open() {
        console.log(this.modal);
        this.modal.open();
    }

    close() {
        this.modal.close();
        if(Cookie.get('showAfterRegisterPopup')) {
            this.router.navigate(['/cards/filters/energyUp']);
        }
    }

    ngOnInit() {

    }


    slideOpen(value) {
        jQuery('.helpModal1 ul li span').removeClass('ah-modal2-spanactive');
        jQuery('.helpModal1 ul li span[data-slide="'+value+'"]').addClass('ah-modal2-spanactive');

        jQuery('.helpModal1 .ah-modal2-wrapper').addClass('hidden');
        jQuery('.helpModal1 .ah-modal2-wrapper[data-slide="'+value+'"]').removeClass('hidden')
    }


    prevSlide(event) {
        let slide = jQuery('.helpModal1 span.ah-modal2-spanactive').attr('data-slide');
        let prev = Number(slide) - 1;
        if(prev >= 1) {
            this.slideOpen(prev);
        }
    }

    nextSlide(event) {
        let slide = jQuery('.helpModal1 span.ah-modal2-spanactive').attr('data-slide');
        let next = Number(slide) + 1;
        if(next <= 3) {
            this.slideOpen(next);
        }
        if(next == 4) {
            this.close();
        }
    }

}
