import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WorkComponent }      from '../components/work.component';
import { WorkCreateComponent }      from '../components/work-create.component';
import { WorkViewComponent }      from '../components/work-view.component';
import {MainComponent} from "../components/main.component";
import {NotFoundComponent} from "../components/not-found.component";
import {HelpComponent} from "../components/help.component";
import { CardsFilterComponent }   from '../components/cards-filter.component';


const routes: Routes = [
    { path: '', component: MainComponent},
    { path: 'paidwin', component: CardsFilterComponent},
    { path: 'cards/filters/:param', component: CardsFilterComponent},
    { path: 'work', component: WorkComponent },
    { path: 'work/:id', component: WorkViewComponent },
    { path: 'work/update/:id', component: WorkCreateComponent },
    { path: 'help', component: HelpComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}
