import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { DragulaModule} from 'ng2-dragula';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from '../components/app.component';
import { WorkComponent } from '../components/work.component';
import { WorkModalComponent } from '../components/work-modal.component';
import { WorkUpdateComponent } from '../components/work-update.component';
import { WorkCreateComponent } from '../components/work-create.component';
import {LoginComponent} from "../components/login.component";
import {CardsCreateComponent} from "../components/cards-create.component";
import {CardsViewComponent} from "../components/cards-view.components";
import {NotFoundComponent} from "../components/not-found.component";
import {HelpComponent} from "../components/help.component";
import {MainComponent} from "../components/main.component";
import {CardsFilterComponent} from "../components/cards-filter.component";
import {HelpModalComponent} from "../components/help-modal.component";
import {HelpModal2Component} from "../components/help-modal2.component";
import {HelpModal3Component} from "../components/help-modal3.component";
import {HelpModal4Component} from "../components/help-modal4.component";
import {PrintModalComponent} from "../components/print-modal.component";
import {PaidWinModalComponent} from "../components/modal/paidwin.component";
import {PaidModalComponent} from "../components/modal/paid.component";
import {AfterRegisterComponent} from "../components/modal/after-register.component";
import {WorkViewComponent} from "../components/work-view.component";


import { FocusTextarea } from '../directives/FocusTextArea';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        Ng2Bs3ModalModule,
        DragulaModule
    ],
    declarations: [
        AppComponent,
        CardsCreateComponent,
        WorkComponent,
        WorkViewComponent,
        WorkCreateComponent,
        WorkModalComponent,
        LoginComponent,
        WorkCreateComponent,
        CardsViewComponent,
        NotFoundComponent,
        HelpComponent,
        MainComponent,
        CardsFilterComponent,
        HelpModalComponent,
        HelpModal2Component,
        HelpModal3Component,
        HelpModal4Component,
        PrintModalComponent,
        WorkUpdateComponent,
        PaidWinModalComponent,
        PaidModalComponent,
        FocusTextarea,
        AfterRegisterComponent
    ],
    providers: [],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
